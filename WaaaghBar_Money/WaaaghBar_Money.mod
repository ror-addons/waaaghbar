<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">	
  <UiMod name="WaaaghBar_Money" version="2.0" date="08/10/2008">		
    <VersionSettings gameVersion="1.9.9" windowsVersion="1.0" savedVariablesVersion="1.0" />		
    <Author name="Hallas" email="warui@hsdev.eu" />		
    <Description text="http://war.curseforge.com/projects/waaaghbar/" />	   		
    <Dependencies>			
      <Dependency name="WaaaghBar" />		
    </Dependencies>   		
    <Files>			
      <File name="WaaaghBar_Money.lua" />		
    </Files>		
    <OnInitialize>			
      <CallFunction name="WaaaghBar.Money.Initialize" />		
    </OnInitialize>	
  </UiMod>
</ModuleFile>