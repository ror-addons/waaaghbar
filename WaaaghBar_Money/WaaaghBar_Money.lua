local Money = {}

function Money.Initialize()
	Money.Waaagh = WaaaghBar.NewPlugin("WaaaghBarMoney")
	Money.Gold = Money.Waaagh:NewElement("Gold", 60, L"000", { r = 255, g = 255, b = 255 }, 00046)
	Money.Silv = Money.Waaagh:NewElement("Silv", 60, L"000", { r = 255, g = 255, b = 255 }, 00047)
	Money.Bras = Money.Waaagh:NewElement("Bras", 60, L"000", { r = 255, g = 255, b = 255 }, 00048)

	-- Register with Events
	RegisterEventHandler(SystemData.Events.PLAYER_MONEY_UPDATED, "WaaaghBar.Money.OnPlayerMoneyUpdated")

	Money.OnPlayerMoneyUpdated()
end

function Money.OnPlayerMoneyUpdated()
	local curMoney = GameData.Player.money
	local g = math.floor (curMoney / 10000)
	local s = math.floor ((curMoney - (g * 10000)) / 100)
	local b = math.mod (curMoney, 100)

	Money.Gold:SetText(towstring(g))
	Money.Silv:SetText(towstring(s))
	Money.Bras:SetText(towstring(b))
end

WaaaghBar.Money = Money
