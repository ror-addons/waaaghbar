local Stats = {
	XpSession = 0,
	RnSession = 0,
	LastXpGain = 0,
	LastRnGain = 0,
	timer = 0,
}

function Stats.Initialize()
	if not (type(WaaaghBarStatsSettings)=="table") then
		WaaaghBarStatsSettings = {
			exp_type = 1,
			rnw_type = 1,
		}
	end
	
	Stats.Waaagh = WaaaghBar.NewPlugin("WaaaghBarStats")
	
	--Exp
	Stats.Exp = Stats.Waaagh:NewElement("Exp", 200, L"Exp", { r = 255, g = 255, b = 255 }, 00052)
	RegisterEventHandler(SystemData.Events.WORLD_OBJ_XP_GAINED, "WaaaghBar.Stats.OnWorldXpGained")
	RegisterEventHandler(SystemData.Events.PLAYER_EXP_UPDATED, "WaaaghBar.Stats.OnPlayerExperienceUpdated")
	WindowRegisterCoreEventHandler(Stats.Exp.name, "OnMouseOver", "WaaaghBar.Stats.OnMouseOverExp") -- Tooltips
	WindowRegisterCoreEventHandler(Stats.Exp.name, "OnRButtonDown", "WaaaghBar.Stats.OnRButtonDownExp")
			
	-- Renown
	Stats.Rnw = Stats.Waaagh:NewElement("Rnw", 200, L"Rnw", { r = 255, g = 255, b = 255 }, 00045)
	RegisterEventHandler(SystemData.Events.PLAYER_RENOWN_UPDATED, "WaaaghBar.Stats.OnPlayerRenownUpdated")
	RegisterEventHandler(SystemData.Events.WORLD_OBJ_RENOWN_GAINED, "WaaaghBar.Stats.OnWorldRnGained")
	WindowRegisterCoreEventHandler(Stats.Rnw.name, "OnMouseOver", "WaaaghBar.Stats.OnMouseOverRnw") -- Tooltips	
	WindowRegisterCoreEventHandler(Stats.Rnw.name, "OnRButtonDown", "WaaaghBar.Stats.OnRButtonDownRnw")
	
	RegisterEventHandler(SystemData.Events.LOADING_END, "WaaaghBar.Stats.CheckLevel")
	RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "WaaaghBar.Stats.CheckLevel")
	
	Stats.OnPlayerRenownUpdated()
	Stats.OnPlayerExperienceUpdated()
end

function Stats.SetDefaultSettings()
	WaaaghBarStatsSettings = {
			exp_type = 1,
			rnw_type = 1,
	}
	Stats.OnPlayerRenownUpdated()
	Stats.OnPlayerExperienceUpdated()
end

function Stats.OnRButtonDownExp()
	if WaaaghBarStatsSettings.exp_type == 3 then
		WaaaghBarStatsSettings.exp_type = 0
	end
	WaaaghBarStatsSettings.exp_type = WaaaghBarStatsSettings.exp_type + 1
	Stats.OnPlayerExperienceUpdated()
end

function Stats.OnRButtonDownRnw()
	if WaaaghBarStatsSettings.rnw_type == 3 then
		WaaaghBarStatsSettings.rnw_type = 0
	end
	WaaaghBarStatsSettings.rnw_type = WaaaghBarStatsSettings.rnw_type + 1
	Stats.OnPlayerRenownUpdated()
end

function Stats.CheckLevel()
	if GameData.Player.level == 40 then
		--local tempwidth = WaaaghBarSettings.WList.width["WaaaghBarStatsExp"]
		--WaaaghBarSettings.WList.width["WaaaghBarStatsExp"] = 0
		WindowSetShowing("WaaaghBarStatsExp", false)
		WaaaghBar.ArrangePlugins()
		--WaaaghBarSettings.WList.width["WaaaghBarStatsExp"] = tempwidth
	else
		WindowSetShowing("WaaaghBarStatsExp", true)
		WaaaghBar.ArrangePlugins()
		Stats.OnPlayerExperienceUpdated()
	end
end

function Stats.OnUpdate(timePassed)
	Stats.timer = Stats.timer + timePassed;
end

function Stats.OnWorldXpGained(id,amount)
	d("OnWorldXpGained Added xp from "..id.." "..amount)
	Stats.XpSession = Stats.XpSession + amount
	Stats.LastXpGain = amount
end

function Stats.OnWorldRnGained(id,amount)
	d("OnWorldXpGained Added renown from "..id.." "..amount)
	Stats.RnSession = Stats.RnSession + amount
	Stats.LastRnGain = amount
end

function Stats.OnTimerReset()
	d("XpSession reset")
	Stats.XpSession = 0
end

function Stats.OnPlayerExperienceUpdated()
	local curExperienceEarned = GameData.Player.Experience.curXpEarned
	local curExperienceNeeded = GameData.Player.Experience.curXpNeeded
	local curExperiencePrcent = curExperienceEarned / curExperienceNeeded * 100
	if WaaaghBarStatsSettings.exp_type == 1 then
		Stats.Exp:SetText(towstring(string.format("%i / %i (%.2f%%)", curExperienceEarned, curExperienceNeeded, curExperiencePrcent)))
	elseif WaaaghBarStatsSettings.exp_type == 2 then
		Stats.Exp:SetText(towstring(string.format("Lv%d (%.2f%%)", GameData.Player.level, curExperiencePrcent)))
	else
		Stats.Exp:SetText(towstring(string.format("Lv%d: %i / %i", GameData.Player.level, curExperienceEarned, curExperienceNeeded)))
	end
end

function Stats.OnPlayerRenownUpdated()
	local curRenownEarned = GameData.Player.Renown.curRenownEarned
	local curRenownNeeded = GameData.Player.Renown.curRenownNeeded
	local curRenownPrcent = curRenownEarned / curRenownNeeded * 100
	if WaaaghBarStatsSettings.rnw_type == 1 then
		Stats.Rnw:SetText(towstring(string.format("%i / %i (%.2f%%)", curRenownEarned, curRenownNeeded, curRenownPrcent)))
	elseif WaaaghBarStatsSettings.rnw_type == 2 then
		Stats.Rnw:SetText(towstring(string.format("Rk%d (%.2f%%)", GameData.Player.Renown.curRank, curRenownPrcent)))
	else
		Stats.Rnw:SetText(towstring(string.format("Rk%d: %i / %i", GameData.Player.Renown.curRank, curRenownEarned, curRenownNeeded)))
	end
	
end

function Stats.OnMouseOverExp()
	local curExperienceEarned = GameData.Player.Experience.curXpEarned
	local curExperienceNeeded = GameData.Player.Experience.curXpNeeded
	local curExperienceEarnPrc = 0
	local curExperienceLeft   = curExperienceNeeded - curExperienceEarned
	local curExperienceLeftPrc = 0
	local curRestBonus = GameData.Player.Experience.restXp
	local curRestBonusPrc = 0
	
	if (curExperienceNeeded > 0) then
		curExperienceEarnPrc = curExperienceEarned / curExperienceNeeded * 100
		curExperienceLeftPrc = curExperienceLeft / curExperienceNeeded * 100
		curRestBonusPrc = curRestBonus / curExperienceNeeded * 100
	end
		
	local xpPerHour = 0
	local timer = Stats.timer
	if (timer > 0) then
		xpPerHour = Stats.XpSession * 3600 / timer
	end

	local timeToLevel = 0
	local textToLevel = L"N/A"
	if (xpPerHour > 0) then
		timeToLevel = (curExperienceNeeded - curExperienceEarned) / (xpPerHour / 3600)
		if (timeToLevel > 0 and timeToLevel < 360000) then
			local ltime, _, _ = TimeUtils.FormatClock(timeToLevel)
			textToLevel = ltime
		end
	end

	Tooltips.CreateTextOnlyTooltip( Stats.Exp.name, nil )
	Tooltips.AnchorTooltip( WaaaghBar.GetTooltipsAnchor() )

	Tooltips.SetTooltipColor( 1, 1, 150, 255, 0 )
	--Tooltips.SetTooltipText( 1, 1, L"LEVEL "..GameData.Player.level..L": "..GameData.Player.activeTitle )
	Tooltips.SetTooltipText( 1, 1, L"Lv. "..GameData.Player.level..L": " )
	Tooltips.SetTooltipText( 2, 1, L"Need:")
	Tooltips.SetTooltipText( 2, 3, towstring(string.format("%10d", curExperienceNeeded)) )
	Tooltips.SetTooltipText( 3, 1, L"Got:")
	Tooltips.SetTooltipText( 3, 3, towstring(string.format("%10d (%.2f%%)", curExperienceEarned, curExperienceEarnPrc)) )
	Tooltips.SetTooltipText( 4, 1, L"More:")
	Tooltips.SetTooltipText( 4, 3, towstring(string.format("%10d (%.2f%%)", curExperienceLeft, curExperienceLeftPrc)) )
	Tooltips.SetTooltipText( 5, 1, L"Exp/h:")
	Tooltips.SetTooltipText( 5, 3, towstring(string.format("%10d", xpPerHour)) )
	Tooltips.SetTooltipText( 6, 1, L"Time:")
	Tooltips.SetTooltipText( 6, 3, L""..textToLevel)
	
	local pos = 7
	if (curRestBonus > 0) then
		for i = 7,9 do
			Tooltips.SetTooltipColor(i, 1, 128, 128, 255)
		end
		Tooltips.SetTooltipText( pos, 1, L"Bonus:")
		Tooltips.SetTooltipText( pos, 3, towstring(string.format("%10d (%.2f%%)", curRestBonus, curRestBonusPrc)) )
		pos = pos + 1
	end
	
	if (Stats.LastXpGain > 0) then
		Tooltips.SetTooltipText( pos, 1, L"Last:")
		Tooltips.SetTooltipText( pos, 3, towstring(string.format("%10d", Stats.LastXpGain)) )
		pos = pos + 1;
		local killsLeft = curExperienceLeft / Stats.LastXpGain
		Tooltips.SetTooltipText( pos, 1, L"Need:" )
		Tooltips.SetTooltipText( pos, 3, towstring(string.format("%10d", killsLeft)) )
	end
	
	Tooltips.Finalize()
end

function Stats.OnMouseOverRnw()
	local curRenownEarned = GameData.Player.Renown.curRenownEarned
	local curRenownNeeded = GameData.Player.Renown.curRenownNeeded
	local curRenownEarnPrc = 0
	local curRenownLeft   = curRenownNeeded - curRenownEarned
	local curRenownLeftPrc = 0
	
	if (curRenownNeeded > 0) then
		curRenownEarnPrc = curRenownEarned / curRenownNeeded * 100
		curRenownLeftPrc = curRenownLeft / curRenownNeeded * 100
	end

	local rnPerHour = 0
	local timer = Stats.timer
	if (timer > 0) then
		rnPerHour = Stats.RnSession * 3600 / timer
	end

	local timeToLevel = 0
	local textToLevel = L"N/A"
	if (rnPerHour > 0) then
		timeToLevel = (curRenownNeeded - curRenownEarned) / (rnPerHour / 3600)
		if (timeToLevel > 0 and timeToLevel < 360000) then
			local ltime, _, _ = TimeUtils.FormatClock(timeToLevel)
			textToLevel = ltime
		end
	end
	
	Tooltips.CreateTextOnlyTooltip( Stats.Rnw.name, nil )
	Tooltips.AnchorTooltip( WaaaghBar.GetTooltipsAnchor() )
	
	Tooltips.SetTooltipColor( 1, 1, 255, 150, 255 )
	
	Tooltips.SetTooltipText( 1, 1, L"RR "..GameData.Player.Renown.curRank..L": "..GameData.Player.Renown.curTitle )
	Tooltips.SetTooltipText( 2, 1, L"Need:")
	Tooltips.SetTooltipText( 2, 3, towstring(string.format("%10d", curRenownNeeded)) )
	Tooltips.SetTooltipText( 3, 1, L"Got:")
	Tooltips.SetTooltipText( 3, 3, towstring(string.format("%10d (%.2f%%)", curRenownEarned, curRenownEarnPrc)) )
	Tooltips.SetTooltipText( 4, 1, L"More:")
	Tooltips.SetTooltipText( 4, 3, towstring(string.format("%10d (%.2f%%)", curRenownLeft, curRenownLeftPrc)) )
	Tooltips.SetTooltipText( 5, 1, L"RR/h:")
	Tooltips.SetTooltipText( 5, 3, towstring(string.format("%10d", rnPerHour)) )
	Tooltips.SetTooltipText( 6, 1, L"Time:")
	Tooltips.SetTooltipText( 6, 3, L""..textToLevel)
	
	local pos = 7
	
	if (Stats.LastRnGain > 0) then
		Tooltips.SetTooltipText( pos, 1, L"Last:")
		Tooltips.SetTooltipText( pos, 3, towstring(string.format("%10d", Stats.LastRnGain)) )
		pos = pos + 1;
		local killsLeft = curRenownLeft / Stats.LastRnGain
		Tooltips.SetTooltipText( pos, 1, L"Need:")
		Tooltips.SetTooltipText( pos, 3, towstring(string.format("%10d", killsLeft)) )
	end
	
	Tooltips.Finalize()
end

WaaaghBar.Stats = Stats