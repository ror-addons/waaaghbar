local Location = {}

function Location.Initialize()
	Location.Waaagh = WaaaghBar.NewPlugin("WaaaghBarLocation")
	Location.Zone = Location.Waaagh:NewElement("Zone", 300, L"Zone", { r = 255, g = 255, b = 255 }, 00111)

	-- Events
	RegisterEventHandler(SystemData.Events.PLAYER_POSITION_UPDATED, "WaaaghBar.Location.OnPlayerPositionUpdated")
end

function Location.OnPlayerPositionUpdated(x, y)
	local zone = GetZoneName(GameData.Player.zone)
	Location.Zone:SetText(towstring(string.format("%s (%.2f, %.2f)", tostring(zone), x / 1000, y / 1000)))
end

WaaaghBar.Location = Location