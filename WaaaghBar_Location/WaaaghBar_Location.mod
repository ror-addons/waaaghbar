<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">	
  <UiMod name="WaaaghBar_Location" version="2.0" date="08/10/2008">		
    <Author name="Hallas" email="warui@hsdev.eu" />		
    <Description text="http://war.curseforge.com/projects/waaaghbar/" />	   		
    <Dependencies>			
      <Dependency name="WaaaghBar" />		
    </Dependencies>   		
    <Files>			
      <File name="WaaaghBar_Location.lua" />		
    </Files>		
    <OnInitialize>			
      <CallFunction name="WaaaghBar.Location.Initialize" />		
    </OnInitialize>	
  </UiMod>
</ModuleFile>