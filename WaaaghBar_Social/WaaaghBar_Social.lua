local Social = {}

local Career = {
[20] = "IRB", -- IronBreaker
[100] = "SWO", -- Swordmaster
[60] = "WIT", -- Witch Hunter
[102] = "LIO", -- White Lion
[62] = "WIZ", -- Bright Wizard
[23] = "ENG", -- Engineer
[101] = "SHD", -- Shadow Warrior
[63] = "WRP", -- Warrior Priest
[103] = "ARC", -- Archmage
[22] = "RUP", -- Rune Priest
[61] = "KBS", -- Knight of the Blazing Sun
[21] = "SLA", -- Slayer

[104] = "BKG", -- Blackguard
[66] = "ZEA", -- Zealot
[26] = "SHA", -- Shaman
[67] = "MAG", -- Magus
[107] = "SOC", -- Sorcerer
[27] = "SQH", -- Squig Herder
[106] = "DOK", -- Disciple of Khaine
[65] = "MAR", -- Marauder
[105] = "WEF", -- Witch Elf
[24] = "ORC", -- Black Orc
[64] = "CHO", -- Chosen
[25] = "CPR", -- Chopper
} 

local getn, ins = table.getn, table.insert

function Social.Initialize()
	Social.Waaagh = WaaaghBar.NewPlugin("WaaaghBarSocial")
	Social.Frnd = Social.Waaagh:NewElement("Frnd", 60, L"000", { r = 255, g = 255, b = 255 }, 00110)
	Social.Gild = Social.Waaagh:NewElement("Gild", 60, L"000", { r = 255, g = 255, b = 255 }, 00166)
	
	-- Events
	RegisterEventHandler(SystemData.Events.GUILD_MEMBER_UPDATED, "WaaaghBar.Social.OnGuildMemberUpdated")
	RegisterEventHandler(SystemData.Events.SOCIAL_FRIENDS_UPDATED, "WaaaghBar.Social.OnSocialFriendsUpdated")
	
	WindowRegisterCoreEventHandler(Social.Frnd.name, "OnMouseOver", "WaaaghBar.Social.OnMouseOverFrnd") -- Tooltips
	WindowRegisterCoreEventHandler(Social.Gild.name, "OnMouseOver", "WaaaghBar.Social.OnMouseOverGild") -- Tooltips	
	WindowRegisterCoreEventHandler(Social.Frnd.name, "OnLButtonUp", "WaaaghBar.Social.OnLButtonUpFrnd")
	WindowRegisterCoreEventHandler(Social.Gild.name, "OnLButtonUp", "WaaaghBar.Social.OnLButtonUpGild")
	
	Social.OnGuildMemberUpdated()
	Social.OnSocialFriendsUpdated()
end

function Social.OnLButtonUpFrnd()
	SocialWindow.ToggleShowing()
end

function Social.OnLButtonUpGild()
	GuildWindow.ToggleShowing()
end

function Social.OnGuildMemberUpdated()
	local guildList, onlineGuildList = GetGuildMemberData(), {}
	for i = 1, getn(guildList) do
		if guildList[i].zoneID ~= 0 then
			ins(onlineGuildList, guildList[i])
		end
	end
	--Social.Gild:SetText(towstring(getn(onlineGuildList) - 1)) -- substract your self
	Social.Gild:SetText(towstring(getn(onlineGuildList))) -- include yourself
end

function Social.OnSocialFriendsUpdated()
	local friendList, onlineFriendList = GetFriendsList(), {}
	for i = 1, getn(friendList) do
		if friendList[i].zoneID ~= 0 then
			ins(onlineFriendList, friendList[i])
		end
	end
	Social.Frnd:SetText(towstring(getn(onlineFriendList)))
end

function Social.OnMouseOverFrnd()

	local friendList, onlineFriendList = GetFriendsList(), {}
	for i = 1, getn(friendList) do --Arrange onlineFriendList
		if friendList[i].zoneID ~= 0 then
			ins(onlineFriendList, friendList[i])
		end
	end
	if getn(onlineFriendList) == 0 then	return end --If no friends online don't show tooltips

	Tooltips.CreateTextOnlyTooltip( Social.Frnd.name, nil)
	Tooltips.AnchorTooltip( WaaaghBar.GetTooltipsAnchor() )
	Tooltips.SetTooltipText( 1, 1, L"FRIENDS:" )
	Tooltips.SetTooltipColor( 1, 1, 0, 150, 255)
	Tooltips.SetTooltipText( 2, 1, L"Total Friends "..towstring(getn(friendList)) )
	Tooltips.SetTooltipColor( 2, 1, 255, 200, 0)
		
	table.sort(onlineFriendList, function(a, b) return (WStringsCompare(a.name, b.name) < 0) end) --Sort the Table
	
	local liststring = L""
	
	for i = 1, getn(onlineFriendList) do
		local friend = onlineFriendList[i]
		local class = towstring(Career[friend.careerID])
		if ( friend.rank < 10 ) then -- Silly one, just for pretty printing
			liststring = liststring..L"Lv "..friend.rank..L"    "..class..L"   "..friend.name
		else
			liststring = liststring..L"Lv"..friend.rank..L"   "..class..L"   "..friend.name	
		end
		--liststring = liststring..L"Lv"..friend.rank..L"   "..friend.name
		if ( i ~= getn(onlineFriendList) ) then
			liststring = liststring..L"\n"
		end		
	end
	
	Tooltips.SetTooltipText( 3, 1, liststring )
	Tooltips.Finalize()
end

function Social.OnMouseOverGild()
	local guildList, onlineGuildList = GetGuildMemberData(), {}
	for i = 1, getn(guildList) do --Arrange onlineGuildList
		if guildList[i].zoneID ~= 0 then
			ins(onlineGuildList, guildList[i])			
		end
	end
	if getn(onlineGuildList) == 0 then return end --If no Guildies online don't show tooltips
	
	Tooltips.CreateTextOnlyTooltip( Social.Gild.name, nil)
	Tooltips.AnchorTooltip( WaaaghBar.GetTooltipsAnchor() )
	Tooltips.SetTooltipText( 1, 1, GameData.Guild.m_GuildName )
	Tooltips.SetTooltipColor( 1, 1, 0, 255, 0)
	Tooltips.SetTooltipText( 2, 1, L"Rank "..GameData.Guild.m_GuildRank..L" ("..towstring(getn(guildList))..L" Members)" )
	Tooltips.SetTooltipColor( 2, 1, 255, 200, 0)
		
	table.sort(onlineGuildList, function(a, b) return (WStringsCompare(a.name, b.name) < 0) end) --Sort the Table
	
	local liststring = L""
	
	for i = 1, getn(onlineGuildList) do
		local guild = onlineGuildList[i]
		local class = towstring(Career[guild.careerID])
		if ( guild.rank < 10 ) then -- Silly one, just for pretty printing
			liststring = liststring..L"Lv "..guild.rank..L"    "..class..L"   "..guild.name
		else
			liststring = liststring..L"Lv"..guild.rank..L"   "..class..L"   "..guild.name
		end
		--liststring = liststring..L"Lv"..guild.rank..L"   "..guild.name
		if ( i ~= getn(onlineGuildList) ) then
			liststring = liststring..L"\n"
		end		
	end
	
	Tooltips.SetTooltipText( 3, 1, liststring )
	Tooltips.Finalize()
end

--function Social.OnMouseOver()
--	local guildList, onlineGuildList = GetGuildMemberData(), {}
--	local friendList, onlineFriendList = GetFriendsList(), {}
--	Tooltips.CreateTextOnlyTooltip( WaaaghBar.Social.Waaagh.name, nil)
--
--	Tooltips.AnchorTooltip( WaaaghBar.GetTooltipsAnchor() )
--	
--	Tooltips.SetTooltipText( 1, 1, L"Friends:")
--	Tooltips.SetTooltipColor( 1, 1, 0, 150, 255)
--	Tooltips.SetTooltipFont( 1, 1, font_clear_medium_bold, left)
--	Tooltips.SetTooltipText( 1, 2, L"Guild:")
--	Tooltips.SetTooltipColor( 1, 2, 0, 255, 0)
--	Tooltips.SetTooltipFont( 1, 2, font_clear_medium_bold, left)
--	
--	for i = 1, getn(guildList) do --Arrange onlineGuildList
--		if guildList[i].zoneID ~= 0 then
--			ins(onlineGuildList, guildList[i])			
--		end
--	end
--	
--	for i = 1, getn(friendList) do --Arrange onlineFriendList
--		if friendList[i].zoneID ~= 0 then
--			ins(onlineFriendList, friendList[i])
--		end
--	end
--	
--	for i = 1, getn(onlineFriendList) do
--		Tooltips.SetTooltipText( i+1, 1, onlineFriendList[i].name )
--	end
--	
--	for i = 1, getn(onlineGuildList) do
--		Tooltips.SetTooltipText( i+1, 2, onlineGuildList[i].name )
--	end
--	
--	Tooltips.Finalize()
--end

WaaaghBar.Social = Social