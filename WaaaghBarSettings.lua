function WaaaghBarSettingsSetDefaultTable()

	WaaaghBarSettings = {
		version = 2.4,
		lock = true,
		PList= {
			name = {
				"WaaaghBarSocial",
				"WaaaghBarLocation",
				"WaaaghBarInf",
				"WaaaghBarStats",
				"WaaaghBarSystem",
				"WaaaghBarPerformance",
				"WaaaghBarMoney",
				"WaaaghBarBagSpace",
			},
			show = {
				["WaaaghBarSocial"] = true,
				["WaaaghBarLocation"] = true,
				["WaaaghBarInf"] = true,
				["WaaaghBarStats"] = true,
				["WaaaghBarSystem"] = true,
				["WaaaghBarPerformance"] = true,
				["WaaaghBarMoney"] = true,
				["WaaaghBarBagSpace"] = true,
			},
		},
		WList = { --Only has Elements
				name = {
					"WaaaghBarSocialFrnd",
					"WaaaghBarSocialGild",
					"WaaaghBarLocationZone",
					"WaaaghBarInfDisplayInf",
					"WaaaghBarStatsExp",
					"WaaaghBarStatsRnw",
					"WaaaghBarSystemClock",
					"WaaaghBarPerformanceFPS",
					"WaaaghBarMoneyGold",
					"WaaaghBarMoneySilv",
					"WaaaghBarMoneyBras",
					"WaaaghBarBagSpaceBagSpace",
				},
				width = {
					["WaaaghBarSocialFrnd"] = 60,
					["WaaaghBarSocialGild"] = 60,
					["WaaaghBarLocationZone"] = 180,
					["WaaaghBarInfDisplayInf"] = 300,
					["WaaaghBarStatsExp"] = 200,
					["WaaaghBarStatsRnw"] = 200,
					["WaaaghBarSystemClock"] = 150,
					["WaaaghBarPerformanceFPS"] = 100,
					["WaaaghBarMoneyGold"] = 60,
					["WaaaghBarMoneySilv"] = 60,
					["WaaaghBarMoneyBras"] = 60,
					["WaaaghBarBagSpaceBagSpace"] = 250,
				},
		},
		["WaaaghBarOne"] = {
			opacity = 0.75,
			font = "font_clear_small_bold",
			OList = {	--Only has Plugins
				name = {
					"WaaaghBarSocial",
					"WaaaghBarLocation",
					"WaaaghBarInf",
					"WaaaghBarStats",
					"WaaaghBarSystem",
					"WaaaghBarPerformance",
					"WaaaghBarMoney",
					"WaaaghBarBagSpace",
				},
				alignment = {
					["WaaaghBarSocial"] = "center",
					["WaaaghBarLocation"] = "center",
					["WaaaghBarInf"] = "center",
					["WaaaghBarStats"] = "center",
					["WaaaghBarSystem"] = "center",
					["WaaaghBarPerformance"] = "center",
					["WaaaghBarMoney"] = "center",
					["WaaaghBarBagSpace"] = "center",
				},
			},
		},
		["WaaaghBarTwo"] = {
			opacity = 0.75,
			font = "font_clear_small_bold",
			OList = {	--Only has Plugins
				name = {
				},
				alignment = {
				},
			},
		},
	}

end