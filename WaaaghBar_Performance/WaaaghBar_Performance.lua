local Performance = {}

local FPS_UPDATE_PER_SEC = 1 -- Update every 1 sec or so free feel to change this to whatever second you like
local Sec

local Frames

local ins, getn = table.insert, table.getn

function Performance.Initialize()
	Sec = 0
	Frames = {}
	Performance.Waaagh = WaaaghBar.NewPlugin("WaaaghBarPerformance")
	Performance.FPS = Performance.Waaagh:NewElement("FPS", 100, L"FPS", { r = 255, g = 255, b = 255 }, 00163)
	
	WindowRegisterCoreEventHandler(Performance.FPS.name, "OnMouseOver", "WaaaghBar.Performance.OnMouseOver") -- Tooltips
end

function Performance.OnUpdate(lastUpdate)
	Sec = Sec + lastUpdate
	if(lastUpdate > 0 and Sec > FPS_UPDATE_PER_SEC) then
		local FPS = math.floor(1 / lastUpdate) -- frequency 1 / s (hz)
		Performance.FPS:SetText(towstring(string.format("%i FPS", FPS)))
		Sec = 0
		if #Frames > 60 then -- 60 mean keep track of only 60sec FPS
			table.remove(Frames,1)
		end
		ins(Frames, FPS)
	end
end

function Performance.GetAvgFPS()
	local minFPS = 99
	local maxFPS = 0
	local totalFPS = 0
	local totalFrames = getn(Frames)
	for i = 1, getn(Frames) do
		totalFPS = totalFPS + Frames[i]
		if Frames[i] > maxFPS then maxFPS = Frames[i] end
		if Frames[i] < minFPS then minFPS = Frames[i] end
	end
	return minFPS, maxFPS, totalFPS / totalFrames
end

function Performance.OnMouseOver()
	local minFPS, maxFPS, avgFPS = Performance.GetAvgFPS()
	Tooltips.CreateTextOnlyTooltip( WaaaghBar.Performance.FPS.name, nil)
    
    Tooltips.AnchorTooltip( WaaaghBar.GetTooltipsAnchor() )
    Tooltips.SetTooltipColor( 1, 1, 64, 224, 208 )
    
    Tooltips.SetTooltipText( 1, 1, L"PERFORMANCE" )
	Tooltips.SetTooltipText( 2, 1, L"Min FPS:" )
	Tooltips.SetTooltipText( 2, 2, towstring(string.format("%5d", minFPS)) )
	Tooltips.SetTooltipText( 3, 1, L"Max FPS:" )
	Tooltips.SetTooltipText( 3, 2, towstring(string.format("%5d", maxFPS)) )
	Tooltips.SetTooltipText( 4, 1, L"Average FPS:" )
	Tooltips.SetTooltipText( 4, 2, towstring(string.format("%5d", avgFPS)) )
	Tooltips.Finalize()
end

WaaaghBar.Performance = Performance