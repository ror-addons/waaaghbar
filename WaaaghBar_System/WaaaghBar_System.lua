--[[
	This is 100% ripped from Aiiane's excellent "Clock" AddOn
  ]]

local System = {
	Clock = {},
}

function System.Initialize()
	System.Waaagh = WaaaghBar.NewPlugin("WaaaghBarSystem")

	-- CLOCK
	System.Clock = System.Waaagh:NewElement("Clock", 150, L"Clock", { r = 255, g = 255, b = 255 }, 00105, { abr = "clock", func = System.OnClockSlash })
	if not WaaaghBarSystemSettings then
        WaaaghBarSystemSettings = {
            hours = 24,
            fmt = "%02d:%02d:%02d",
        }
    end
    
 	-- Events   
    WindowRegisterCoreEventHandler(System.Clock.name, "OnRButtonDown", "WaaaghBar.System.OnRButtonDown")
end

function System.SetDefaultSettings()
	WaaaghBarSystemSettings = {
            hours = 24,
            fmt = "%02d:%02d:%02d",
    }
end

function System.OnRButtonDown()
	if WaaaghBarSystemSettings.hours == 24 then
		WaaaghBarSystemSettings.hours = 12
	else
		WaaaghBarSystemSettings.hours = 24
	end	
end

function System.OnClockSlash(opt, val)
	if	   opt == "hours" then
		if	   val == "12" then
			WaaaghBarSystemSettings.hours = 12
		elseif val == "24" then
			WaaaghBarSystemSettings.hours = 24
		end
	elseif opt == "format" then
		WaaaghBarSystemSettings.fmt = val
	end
end

function System.OnUpdate(elapsedTime)
   if System.lastUpdate == nil then
        local lastEntry = TextLogGetNumEntries("Chat") - 1
        if lastEntry >= 0 then
            local lastTime = TextLogGetEntry("Chat", lastEntry)
            local hours,mins,secs = lastTime:match(L"([0-9]+):([0-9]+):([0-9]+)")
            System.hour = tonumber(hours)
            System.min = tonumber(mins)
            System.sec = tonumber(secs)
            System.lastUpdate = lastTime
        else
            return -- Haven't gotten a combat log entry yet
        end
    else
        local lastEntry = TextLogGetNumEntries("Chat") - 1
        local lastTime = TextLogGetEntry("Chat", lastEntry)
        if lastTime ~= System.lastUpdate then
            System.lastUpdate = lastTime
            local hours, mins, secs = lastTime:match(L"([0-9]+):([0-9]+):([0-9]+)")
            System.hour = tonumber(hours)
            System.min = tonumber(mins)
            System.sec = tonumber(secs)
            System.lastUpdate = lastTime
        else
            System.sec = System.sec + elapsedTime
            while System.sec >= 60 do
                System.min = System.min + 1
                System.sec = System.sec - 60
            end
            while System.min >= 60 do
                System.hour = System.hour + 1
                System.min = System.min - 60
            end
            if System.hour >= 24 then System.hour = (System.hour % 24) end
        end
    end

	local fmt = WaaaghBarSystemSettings.fmt
    if WaaaghBarSystemSettings.hours == 12 then
        if System.hour < 12 then fmt=fmt.." AM" else fmt=fmt.." PM" end
    end
    
    local h = System.hour % WaaaghBarSystemSettings.hours
    if h == 0 and WaaaghBarSystemSettings.hours == 12 then
    	h = 12
    end
    System.Clock:SetText(towstring(string.format(fmt, h, System.min, System.sec)))
end

WaaaghBar.System = System