<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">	
  <UiMod name="WaaaghBar_System" version="2.1" date="08/10/2008">		
    <VersionSettings gameVersion="1.9.9" windowsVersion="1.0" savedVariablesVersion="1.0" />		
    <Author name="Hallas" email="warui@hsdev.eu" />		
    <Description text="http://war.curseforge.com/projects/waaaghbar/" />	   		
    <Dependencies>			
      <Dependency name="WaaaghBar" />		
    </Dependencies>   		
    <Files>			
      <File name="WaaaghBar_System.lua" />		
    </Files>		
    <OnInitialize>			
      <CallFunction name="WaaaghBar.System.Initialize" />		
    </OnInitialize>		
    <OnUpdate>			
      <CallFunction name="WaaaghBar.System.OnUpdate" />		
    </OnUpdate>        
    <SavedVariables>            
      <SavedVariable name="WaaaghBarSystemSettings" />        
    </SavedVariables>	
  </UiMod>
</ModuleFile>