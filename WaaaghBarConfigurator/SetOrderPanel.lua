if not WaaaghBarConfigurator then return end
local LibGUI = LibStub("LibGUI")
local CreatePanel, UpdatePanel, ApplyPanel, CheckInput
local panel = {}

local ins, getn = table.insert, table.getn
local BAR_ONE = "WaaaghBarOne"
local BAR_TWO = "WaaaghBarTwo"
local TOTAL_E = getn(WaaaghBarSettings[BAR_ONE].OList.name) + getn(WaaaghBarSettings[BAR_TWO].OList.name)

-- Our panel window
local W

local LL --Label List
local FTB --FixedTextBox List
local AAL --Arrow Label List
local TB --TextBox List

panel.title = L"3. SetOrder"

-- Create the panel
function CreatePanel(self)
    -- Do we somehow have a window still around? If so, we're done, that was easy.
    if W and W.name then EA_ChatWindow.Print(L"Got Already!") return W end
    
    W = LibGUI("Blackframe")
    --W:Resize(500, 250)

    LL = {}
    FTB = {}
    ALL = {}
    TB = {}
        
    local e
    
    -- Title label
    e = W("Label")
    e:Resize(480)
    e:AnchorTo(W, "top", "top", 0, 10)
    e:Font("font_clear_medium_bold")
    e:SetText(L"SetOrder Options")
    W.Title = e
    
    e = W("Optionbutton")
	e:Position(200, 60)
    e:RegisterEvent("OnLButtonUp")
    W.Optionbutton1 = e
    e = W("Label")
    e:Resize(50)
    e:AnchorTo(W.Optionbutton1, "left", "right", 10, 0)
    e:SetText(L"1")
    
    e = W("Optionbutton")
    e:Position(300, 60)
    e:RegisterEvent("OnLButtonUp")
    W.Optionbutton2 = e
    e = W("Label")
    e:Resize(50)
    e:AnchorTo(W.Optionbutton2, "left", "right", 10, 0)
    e:SetText(L"2") 
    
    local y = 70
    for i = 1, TOTAL_E do
    	y = y + 40
        -- Plugin Label
        e = W("Label")
    	e:Resize(180)
    	e:Position(10,y)
    	e:Align("rightcenter")
    	ins(LL, e)
    
	    -- Plugin Textbox Display Order CANNOT EDIT
	    e = W("Textbox")
	    e:Resize(50)
	    e:AnchorTo(LL[i], "right", "left", 40, 0)
	    e:IgnoreInput()
	    ins(FTB, e)
	    
	    e = W("Label")
	    e:Resize(50)
	    e:AnchorTo(LL[i], "right", "left", 90, 0)
	    e:SetText(L"->")
	    ins(ALL, e)
	    
	    -- New Plugin Textbow Display Order
	    e = W("Textbox")
	    e:Resize(50)
	    e:AnchorTo(LL[i], "right", "left", 140, 0)
	    ins(TB, e)
    end
    
    W.Optionbutton1.OnLButtonUp = function()
    	W.Optionbutton2:Clear()
   		for i = 1, getn(WaaaghBarSettings[BAR_ONE].OList.name) do
			LL[i]:SetText( string.sub(WaaaghBarSettings[BAR_ONE].OList.name[i], 10) )
			LL[i]:Show()
		    FTB[i]:SetText(i)
		    FTB[i]:Show()
		    ALL[i]:Show()
		    TB[i]:SetText(i)
		    TB[i]:Show()
		end
		for i = getn(WaaaghBarSettings[BAR_ONE].OList.name)+1, TOTAL_E do
			LL[i]:Hide()
			FTB[i]:Hide()
			ALL[i]:Hide()
		    TB[i]:Hide()
		end
		W:Resize(500, 70 + getn(WaaaghBarSettings[BAR_ONE].OList.name) * 40 + 180)
    end
    
    W.Optionbutton2.OnLButtonUp = function()
    	W.Optionbutton1:Clear()
		for i = 1, getn(WaaaghBarSettings[BAR_TWO].OList.name) do
			LL[i]:SetText( string.sub(WaaaghBarSettings[BAR_TWO].OList.name[i], 10) )
			LL[i]:Show()
		    FTB[i]:SetText(i)
		    FTB[i]:Show()
		    ALL[i]:Show()
		    TB[i]:SetText(i)
		    TB[i]:Show()
		end
		for i = getn(WaaaghBarSettings[BAR_TWO].OList.name)+1, TOTAL_E do
			LL[i]:Hide()
			FTB[i]:Hide()
			ALL[i]:Hide()
		    TB[i]:Hide()
		end
		W:Resize(500, 70 + getn(WaaaghBarSettings[BAR_TWO].OList.name) * 40 + 180)
    end
    
    -- Display Msg
    e = W("Label")
    e:Resize(500)
    e:Align("center")
    e:AnchorTo(W, "bottomleft", "bottomleft", 0, -80)
    e:SetText("Setting Order will reset Alignment to Center")
    W.Msg = e
    
    -- Apply button
    e = W("Button")
    e:Resize(200)
    e:SetText(L"Apply")
    e:AnchorTo(W, "bottomleft", "bottomleft", 40, -20)
    e.OnLButtonUp = function() ApplyPanel() end
    W.ButtonApply = e
    
    -- Revert button
    e = W("Button")
    e:Resize(200)
    e:SetText(L"Revert")
    e:AnchorTo(W, "bottomright", "bottomright", -40, -20)
    e.OnLButtonUp = function() UpdatePanel() end
    W.ButtonRevert = e
    
    return W
end

function CheckInput()
	if W.Optionbutton1:GetValue() then
		for i = 1, getn(WaaaghBarSettings[BAR_ONE].OList.name) do
			local x = tonumber(TB[i]:GetText())
			if x < 1 or x > getn(TB) then
				return false
			end
		end
	elseif W.Optionbutton2:GetValue() then
		for i = 1, getn(WaaaghBarSettings[BAR_TWO].OList.name) do
			local x = tonumber(TB[i]:GetText())
			if x < 1 or x > getn(TB) then
				return false
			end
		end
	end
	return true
end

function ApplyPanel()
	if CheckInput() then
		W.Msg:SetText( "" )
		local neworder = {}
		if W.Optionbutton1:GetValue() then
			for i = 1, getn(WaaaghBarSettings[BAR_ONE].OList.name) do
				for j = 1, getn(TB) do
					if (math.floor(TB[j]:GetText()) == i) then
						ins(neworder, WaaaghBarSettings[BAR_ONE].OList.name[j])
						WaaaghBarSettings[BAR_ONE].OList.alignment[WaaaghBarSettings[BAR_ONE].OList.name[j]] = "center"
					end
				end
			end
		    WaaaghBarSettings[BAR_ONE].OList.name = neworder
		elseif W.Optionbutton2:GetValue() then
			for i = 1, getn(WaaaghBarSettings[BAR_TWO].OList.name) do
				for j = 1, getn(TB) do
					if (math.floor(TB[j]:GetText()) == i) then
						ins(neworder, WaaaghBarSettings[BAR_TWO].OList.name[j])
						WaaaghBarSettings[BAR_TWO].OList.alignment[WaaaghBarSettings[BAR_TWO].OList.name[j]] = "center"
					end
				end
			end
		    WaaaghBarSettings[BAR_TWO].OList.name = neworder
		end
	    WaaaghBar.ArrangePlugins()
    else
    	W.Msg:SetText( "Input Error!" )
    	EA_ChatWindow.Print( L"WaaaghBar Error: Input must be number and within range!" )
    end
    UpdatePanel()
end

function UpdatePanel(self, key, value)
	if W.Optionbutton1:GetValue() or (not W.Optionbutton1:GetValue() and not W.Optionbutton2:GetValue()) then
		W.Optionbutton1:Check()
		W.Optionbutton2:Clear()
		for i = 1, getn(WaaaghBarSettings[BAR_ONE].OList.name) do
			LL[i]:SetText( string.sub(WaaaghBarSettings[BAR_ONE].OList.name[i], 10) )
			LL[i]:Show()
		    FTB[i]:SetText(i)
		    FTB[i]:Show()
		    ALL[i]:Show()
		    TB[i]:SetText(i)
		    TB[i]:Show()
		end
		for i = getn(WaaaghBarSettings[BAR_ONE].OList.name)+1, TOTAL_E do
			LL[i]:Hide()
			FTB[i]:Hide()
			ALL[i]:Hide()
		    TB[i]:Hide()
		end
		W:Resize(500, 70 + getn(WaaaghBarSettings[BAR_ONE].OList.name) * 40 + 180)
	elseif W.Optionbutton2:GetValue() then
		for i = 1, getn(WaaaghBarSettings[BAR_TWO].OList.name) do
			LL[i]:SetText( string.sub(WaaaghBarSettings[BAR_TWO].OList.name[i], 10) )
			LL[i]:Show()
		    FTB[i]:SetText(i)
		    FTB[i]:Show()
		    ALL[i]:Show()
		    TB[i]:SetText(i)
		    TB[i]:Show()
		end
		for i = getn(WaaaghBarSettings[BAR_TWO].OList.name)+1, TOTAL_E do
			LL[i]:Hide()
			FTB[i]:Hide()
			ALL[i]:Hide()
		    TB[i]:Hide()
		end
		W:Resize(500, 70 + getn(WaaaghBarSettings[BAR_TWO].OList.name) * 40 + 180)
	end
end

-- Actually add the panel
panel.create = CreatePanel
panel.update = UpdatePanel
panel.id = WaaaghBarConfigurator.AddPanel(panel)