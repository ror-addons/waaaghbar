if not WaaaghBarConfigurator then return end
local LibGUI = LibStub("LibGUI")
local CreatePanel, UpdatePanel, ApplyPanel
local panel = {}

local ins, getn = table.insert, table.getn
local BAR_ONE = "WaaaghBarOne"
local BAR_TWO = "WaaaghBarTwo"

-- Our panel window
local W

panel.title = L"General"

-- Create the panel
function CreatePanel(self)
    -- Do we somehow have a window still around? If so, we're done, that was easy.
    if W and W.name then return W end
    
    W = LibGUI("Blackframe")
    W:Resize(500, 330)
    
    local e
    
    -- Title label
    e = W("Label")
    e:Resize(480)
    e:AnchorTo(W, "top", "top", 0, 10)
    e:Font("font_clear_medium_bold")
    e:SetText(L"General Settings")
    W.Title = e
    
    e = W("Optionbutton")
	e:Position(200, 60)
    e:RegisterEvent("OnLButtonUp")
    W.Optionbutton1 = e
    e = W("Label")
    e:Resize(50)
    e:AnchorTo(W.Optionbutton1, "left", "right", 10, 0)
    e:SetText(L"1")
    
    e = W("Optionbutton")
    e:Position(300, 60)
    e:RegisterEvent("OnLButtonUp")
    W.Optionbutton2 = e
    e = W("Label")
    e:Resize(50)
    e:AnchorTo(W.Optionbutton2, "left", "right", 10, 0)
    e:SetText(L"2") 

    -- Opacity label
    e = W("Label")
    e:Resize(100)
    e:Position(70,100)
    e:SetText(L"Alpha")
    
	e = W("Slider")
	e:Position(220,100)
	e:SetRange(0, 1)
	W.Slider = e
    
	-- Font label
	e = W("Label")
	e:Resize(100)
	e:Position(70,150)
	e:SetText(L"Font")
	
	e = W("Combobox")
	e:Position(200,150)
	e:Add("font_clear_small_bold")
	e:Add("font_clear_medium_bold")
	e:Add("font_clear_large_bold")
	e:Add("font_default_text_small")
	e:Add("font_default_text_large")
	W.Combobox = e
	
	-- Bar Selection Event Function
	W.Optionbutton1.OnLButtonUp = function()
    	W.Optionbutton2:Clear()
    	UpdatePanel()
    end
    
    W.Optionbutton2.OnLButtonUp = function()
    	W.Optionbutton1:Clear()
    	UpdatePanel()
    end
	
    e = W("Button")
    e:Resize(230)
    e:SetText(L"Reset Plugins Settings")
    e:AnchorTo(W, "center", "center", 0, 60)
    e.OnLButtonUp = function() LoadDefault() end
    W.ButtonLoadDefault = e
    
    -- Apply button
    e = W("Button")
    e:Resize(200)
    e:SetText(L"Apply")
    e:AnchorTo(W, "bottomleft", "bottomleft", 40, -20)
    e.OnLButtonUp = function() ApplyPanel() end
    W.ButtonApply = e
    
    -- Revert button
    e = W("Button")
    e:Resize(200)
    e:SetText(L"Revert")
    e:AnchorTo(W, "bottomright", "bottomright", -40, -20)
    e.OnLButtonUp = function() UpdatePanel() end
    W.ButtonRevert = e
    
    return W
end

function LoadDefault()
	WaaaghBar.BagSpace.SetDefaultSettings()
	WaaaghBar.Location.SetDefaultSettings()
	WaaaghBar.Stats.SetDefaultSettings()
	WaaaghBar.System.SetDefaultSettings()
	WaaaghBar.WInfluence.SetDefaultSettings()
end

function ApplyPanel()
	if W.Optionbutton1:GetValue() then
		WaaaghBarSettings[BAR_ONE].opacity = W.Slider:GetValue()
		WaaaghBarSettings[BAR_ONE].font = W.Combobox:Selected()
		WindowSetAlpha(BAR_ONE.."Background", WaaaghBarSettings[BAR_ONE].opacity)
		WaaaghBar.SetBarFont(BAR_ONE, tostring(WaaaghBarSettings[BAR_ONE].font))
	elseif W.Optionbutton2:GetValue() then
		WaaaghBarSettings[BAR_TWO].opacity = W.Slider:GetValue()
		WaaaghBarSettings[BAR_TWO].font = W.Combobox:Selected()
		WindowSetAlpha(BAR_TWO.."Background", WaaaghBarSettings[BAR_TWO].opacity)
		WaaaghBar.SetBarFont(BAR_TWO, tostring(WaaaghBarSettings[BAR_TWO].font))
	end	
end

function UpdatePanel(self, key, value)
	if W.Optionbutton1:GetValue() or (not W.Optionbutton1:GetValue() and not W.Optionbutton2:GetValue()) then
		W.Optionbutton1:Check()
		W.Optionbutton2:Clear()
		W.Slider:SetValue(WaaaghBarSettings[BAR_ONE].opacity)
		W.Combobox:Select(WaaaghBarSettings[BAR_ONE].font)
	elseif W.Optionbutton2:GetValue() then
		W.Slider:SetValue(WaaaghBarSettings[BAR_TWO].opacity)
		W.Combobox:Select(WaaaghBarSettings[BAR_TWO].font)
	end
end

-- Actually add the panel
panel.create = CreatePanel
panel.update = UpdatePanel
panel.id = WaaaghBarConfigurator.AddPanel(panel)