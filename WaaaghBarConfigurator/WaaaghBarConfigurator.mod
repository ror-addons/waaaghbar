<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">	
  <UiMod name="WaaaghBarConfigurator" version="1.0" date="12/11/2008" >		
    <VersionSettings gameVersion="1.9.9" windowsVersion="1.0" savedVariablesVersion="1.0" />		
    <Author name="Long" email="long_ling@yahoo.com" />		
    <Description text="Graphical configuration interface for WaaaghBar." />                 
    <Dependencies>            
      <Dependency name="WaaaghBar" />        
    </Dependencies>         		
    <Files>            
      <File name="LibStub.lua" />            
      <File name="LibGUI.lua" />            
      <File name="WaaaghBarConfigurator.lua" />                         
      <!-- Panels -->             
      <File name="SettingsPanel.lua" />            
      <File name="SetWidthPanel.lua" />            
      <File name="SetEnablePanel.lua" />            
      <File name="SetBarPanel.lua" />            
      <File name="SetOrderPanel.lua" />            
      <File name="SetAlignmentPanel.lua" />		
    </Files>		 		
    <OnInitialize>            
      <CallFunction name="WaaaghBarConfigurator.Initialize" />		
    </OnInitialize>		
    <OnShutdown/>		 	
  </UiMod>
</ModuleFile>