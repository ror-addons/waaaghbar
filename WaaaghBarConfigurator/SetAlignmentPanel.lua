if not WaaaghBarConfigurator then return end
local LibGUI = LibStub("LibGUI")
local CreatePanel, UpdatePanel, ApplyPanel
local panel = {}

local ins, getn = table.insert, table.getn
local BAR_ONE = "WaaaghBarOne"
local BAR_TWO = "WaaaghBarTwo"
local TOTAL_E = getn(WaaaghBarSettings[BAR_ONE].OList.name) + getn(WaaaghBarSettings[BAR_TWO].OList.name)

-- Our panel window
local W

local LL --Label List
local OLL --Option Left List
local OCL --Option center List
local ORL --Option right list

panel.title = L"4. SetAlignment"

-- Create the panel
function CreatePanel(self)
    -- Do we somehow have a window still around? If so, we're done, that was easy.
    if W and W.name then EA_ChatWindow.Print(L"Got Already!") return W end
    
    W = LibGUI("Blackframe")
    --W:Resize(500, 250)

    LL = {}
    OLL = {}
    OCL = {}
    ORL = {}
        
    local e
    
    -- Title label
    e = W("Label")
    e:Resize(480)
    e:AnchorTo(W, "top", "top", 0, 10)
    e:Font("font_clear_medium_bold")
    e:SetText(L"SetAlignment Options")
    W.Title = e

    e = W("Optionbutton")
	e:Position(200, 60)
    e:RegisterEvent("OnLButtonUp")
    W.Optionbutton1 = e
    e = W("Label")
    e:Resize(50)
    e:AnchorTo(W.Optionbutton1, "left", "right", 10, 0)
    e:SetText(L"1")
    
    e = W("Optionbutton")
    e:Position(300, 60)
    e:RegisterEvent("OnLButtonUp")
    W.Optionbutton2 = e
    e = W("Label")
    e:Resize(50)
    e:AnchorTo(W.Optionbutton2, "left", "right", 10, 0)
    e:SetText(L"2") 
    
    e = W("Label")
    e:Resize(100)
    e:Position(200,85)
    e:SetText("Left")
    
    e = W("Label")
    e:Resize(100)
    e:Position(260,85)
    e:SetText("Center")
    
    e = W("Label")
    e:Resize(100)
    e:Position(320,85)
    e:SetText("right")
    
    local y = 70
    local name 
    for i = 1, TOTAL_E do
    	y = y + 40
        -- Plugin Label
        e = W("Label")
    	e:Resize(180)
    	e:Position(10,y)
    	e:Align("rightcenter")
    	ins(LL, e)
    	
    	-- Optionbutton Group
	    e = W("Optionbutton")
	    e:AnchorTo(LL[i], "right", "left", 50, 0)
	    e:RegisterEvent("OnLButtonUp")
	    ins(OLL, e)
	    
	    e = W("Optionbutton")
	    e:AnchorTo(LL[i], "right", "left", 110, 0)
	    e:RegisterEvent("OnLButtonUp")
	    ins(OCL, e)
	    
	    e = W("Optionbutton")
	    e:AnchorTo(LL[i], "right", "left", 170, 0)
	    e:RegisterEvent("OnLButtonUp")
	    ins(ORL, e)
	    
	    -- Define the Event Function to force one selection only
	    OLL[i].OnLButtonUp = function()
	    	for j = 1, TOTAL_E do
	    		OLL[j]:Check()
	    		OCL[j]:Clear()
	    		ORL[j]:Clear()
	    	end
	    end
	    OCL[i].OnLButtonUp = function()
		    for j = i, TOTAL_E do
		    	OLL[j]:Clear()
		    	OCL[j]:Check()
		    	ORL[j]:Clear()
		    end
		    for k = 1, i do
	    		if ORL[k]:GetValue() then
	    			OLL[k]:Clear()
	    			OCL[k]:Check()
	    			ORL[k]:Clear()
	    		end
	    	end
	    end	    
	    ORL[i].OnLButtonUp = function()
		    for j = i, TOTAL_E do
				OLL[j]:Clear()
		    	OCL[j]:Clear()
		    	ORL[j]:Check()
		    end
	    end
	end
	
	-- Bar Selection Event Function
	W.Optionbutton1.OnLButtonUp = function()
    	W.Optionbutton2:Clear()
   		for i = 1, getn(WaaaghBarSettings[BAR_ONE].OList.name) do
			LL[i]:SetText( string.sub(WaaaghBarSettings[BAR_ONE].OList.name[i], 10) )
			LL[i]:Show()
			OLL[i]:Show()
			OCL[i]:Show()
			ORL[i]:Show()
			
			OLL[i]:Clear()
			OCL[i]:Clear()
			ORL[i]:Clear()
			if WaaaghBarSettings[BAR_ONE].OList.alignment[WaaaghBarSettings[BAR_ONE].OList.name[i]] == "left" then
				OLL[i]:Check()
			elseif WaaaghBarSettings[BAR_ONE].OList.alignment[WaaaghBarSettings[BAR_ONE].OList.name[i]] == "center" then
				OCL[i]:Check()
			elseif WaaaghBarSettings[BAR_ONE].OList.alignment[WaaaghBarSettings[BAR_ONE].OList.name[i]] == "right" then
				ORL[i]:Check()
			end
		end
		for i = getn(WaaaghBarSettings[BAR_ONE].OList.name)+1, TOTAL_E do
			LL[i]:Hide()
			OLL[i]:Hide()
			OCL[i]:Hide()
			ORL[i]:Hide()
		end
		W:Resize(500, 70 + getn(WaaaghBarSettings[BAR_ONE].OList.name) * 40 + 180)
    end
    
    W.Optionbutton2.OnLButtonUp = function()
    	W.Optionbutton1:Clear()
		for i = 1, getn(WaaaghBarSettings[BAR_TWO].OList.name) do
			LL[i]:SetText( string.sub(WaaaghBarSettings[BAR_TWO].OList.name[i], 10) )
			LL[i]:Show()
		    OLL[i]:Show()
			OCL[i]:Show()
			ORL[i]:Show()
			
			OLL[i]:Clear()
			OCL[i]:Clear()
			ORL[i]:Clear()
			if WaaaghBarSettings[BAR_TWO].OList.alignment[WaaaghBarSettings[BAR_TWO].OList.name[i]] == "left" then
				OLL[i]:Check()
			elseif WaaaghBarSettings[BAR_TWO].OList.alignment[WaaaghBarSettings[BAR_TWO].OList.name[i]] == "center" then
				OCL[i]:Check()
			elseif WaaaghBarSettings[BAR_TWO].OList.alignment[WaaaghBarSettings[BAR_TWO].OList.name[i]] == "right" then
				ORL[i]:Check()
			end
		end
		for i = getn(WaaaghBarSettings[BAR_TWO].OList.name)+1, TOTAL_E do
			LL[i]:Hide()
			OLL[i]:Hide()
			OCL[i]:Hide()
			ORL[i]:Hide()
		end
		W:Resize(500, 70 + getn(WaaaghBarSettings[BAR_TWO].OList.name) * 40 + 180)
    end
    
    --Display Msg
    e = W("Label")
    e:Resize(500)
    e:AnchorTo(W, "bottomleft", "bottomleft", 0, -80)
    e:Align("center")
    e:SetText(L"Tips: Set Left first, then Center, and Right!")
    W.Msg = e
    
--    W:Resize(500, y+180) -- 120 is the height of button
	W:Resize(500, y+170)
    
    -- Apply button
    e = W("Button")
    e:Resize(200)
    e:SetText(L"Apply")
    e:AnchorTo(W, "bottomleft", "bottomleft", 40, -20)
    e.OnLButtonUp = function() ApplyPanel() end
    W.ButtonApply = e
    
    -- Revert button
    e = W("Button")
    e:Resize(200)
    e:SetText(L"Revert")
    e:AnchorTo(W, "bottomright", "bottomright", -40, -20)
    e.OnLButtonUp = function() UpdatePanel() end
    W.ButtonRevert = e
    
    return W
end

function ApplyPanel()
	if W.Optionbutton1:GetValue() then
		for i = 1, getn(WaaaghBarSettings[BAR_ONE].OList.name) do
			if OLL[i]:GetValue() then
				WaaaghBarSettings[BAR_ONE].OList.alignment[WaaaghBarSettings[BAR_ONE].OList.name[i]] = "left"
			elseif OCL[i]:GetValue() then
				WaaaghBarSettings[BAR_ONE].OList.alignment[WaaaghBarSettings[BAR_ONE].OList.name[i]] = "center"
			elseif ORL[i]:GetValue() then
				WaaaghBarSettings[BAR_ONE].OList.alignment[WaaaghBarSettings[BAR_ONE].OList.name[i]] = "right"
			end
		end
	elseif W.Optionbutton2:GetValue() then
		for i = 1, getn(WaaaghBarSettings[BAR_TWO].OList.name) do
			if OLL[i]:GetValue() then
				WaaaghBarSettings[BAR_TWO].OList.alignment[WaaaghBarSettings[BAR_TWO].OList.name[i]] = "left"
			elseif OCL[i]:GetValue() then
				WaaaghBarSettings[BAR_TWO].OList.alignment[WaaaghBarSettings[BAR_TWO].OList.name[i]] = "center"
			elseif ORL[i]:GetValue() then
				WaaaghBarSettings[BAR_TWO].OList.alignment[WaaaghBarSettings[BAR_TWO].OList.name[i]] = "right"
			end
		end
	end
	WaaaghBar.ArrangePlugins()
end

function UpdatePanel(self, key, value)
	if W.Optionbutton1:GetValue() or (not W.Optionbutton1:GetValue() and not W.Optionbutton2:GetValue()) then
		W.Optionbutton1:Check()
		W.Optionbutton2:Clear()
		for i = 1, getn(WaaaghBarSettings[BAR_ONE].OList.name) do
			LL[i]:SetText( string.sub(WaaaghBarSettings[BAR_ONE].OList.name[i], 10) )
			LL[i]:Show()
			OLL[i]:Show()
			OCL[i]:Show()
			ORL[i]:Show()
			
			OLL[i]:Clear()
			OCL[i]:Clear()
			ORL[i]:Clear()
			if WaaaghBarSettings[BAR_ONE].OList.alignment[WaaaghBarSettings[BAR_ONE].OList.name[i]] == "left" then
				OLL[i]:Check()
			elseif WaaaghBarSettings[BAR_ONE].OList.alignment[WaaaghBarSettings[BAR_ONE].OList.name[i]] == "center" then
				OCL[i]:Check()
			elseif WaaaghBarSettings[BAR_ONE].OList.alignment[WaaaghBarSettings[BAR_ONE].OList.name[i]] == "right" then
				ORL[i]:Check()
			end
		end
		for i = getn(WaaaghBarSettings[BAR_ONE].OList.name)+1, TOTAL_E do
			LL[i]:Hide()
			OLL[i]:Hide()
			OCL[i]:Hide()
			ORL[i]:Hide()
		end
		W:Resize(500, 70 + getn(WaaaghBarSettings[BAR_ONE].OList.name) * 40 + 180)
	elseif W.Optionbutton2:GetValue() then
		for i = 1, getn(WaaaghBarSettings[BAR_TWO].OList.name) do
			LL[i]:SetText( string.sub(WaaaghBarSettings[BAR_TWO].OList.name[i], 10) )
			LL[i]:Show()
		    OLL[i]:Show()
			OCL[i]:Show()
			ORL[i]:Show()
			
			OLL[i]:Clear()
			OCL[i]:Clear()
			ORL[i]:Clear()
			if WaaaghBarSettings[BAR_TWO].OList.alignment[WaaaghBarSettings[BAR_TWO].OList.name[i]] == "left" then
				OLL[i]:Check()
			elseif WaaaghBarSettings[BAR_TWO].OList.alignment[WaaaghBarSettings[BAR_TWO].OList.name[i]] == "center" then
				OCL[i]:Check()
			elseif WaaaghBarSettings[BAR_TWO].OList.alignment[WaaaghBarSettings[BAR_TWO].OList.name[i]] == "right" then
				ORL[i]:Check()
			end
		end
		for i = getn(WaaaghBarSettings[BAR_TWO].OList.name)+1, TOTAL_E do
			LL[i]:Hide()
			OLL[i]:Hide()
			OCL[i]:Hide()
			ORL[i]:Hide()
		end
		W:Resize(500, 70 + getn(WaaaghBarSettings[BAR_TWO].OList.name) * 40 + 180)
	end
end

-- Actually add the panel
panel.create = CreatePanel
panel.update = UpdatePanel
panel.id = WaaaghBarConfigurator.AddPanel(panel)