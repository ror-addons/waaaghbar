if not WaaaghBarConfigurator then return end
local LibGUI = LibStub("LibGUI")
local CreatePanel, UpdatePanel, ApplyPanel
local panel = {}

local ins, getn = table.insert, table.getn
local BAR_ONE = "WaaaghBarOne"
local BAR_TWO = "WaaaghBarTwo"

-- Our panel window
local W

local LL = {} --Label List
local TB = {} --TextBox List

panel.title = L"SetWidth"

-- Create the panel
function CreatePanel(self)
    -- Do we somehow have a window still around? If so, we're done, that was easy.
    if W and W.name then return W end
    
    W = LibGUI("Blackframe")
    --W:Resize(500, 250)
    
    local e
    
    -- Title label
    e = W("Label")
    e:Resize(480)
    e:AnchorTo(W, "top", "top", 0, 10)
    e:Font("font_clear_medium_bold")
    e:SetText(L"SetWidth Options")
    W.Title = e

    e = W("Label")
    e:Resize(100)
    e:Position(265, 60)
    e:SetText(L"Widths")
    
    local y = 50
    local name 
    for i = 1, getn(WaaaghBarSettings.WList.name) do
    	name = string.sub(WaaaghBarSettings.WList.name[i], 10)
    	y = y + 40
        -- Element Label
        e = W("Label")
    	e:Resize(180)
    	e:Position(60,y)
    	e:Align("rightcenter")
    	e:SetText(name)
    	--W.LabelWidth = e
    	ins(LL, e)
    
	    -- Element Textbox
	    e = W("Textbox")
	    e:Resize(50)
	    e:AnchorTo(LL[i], "right", "left", 50, 0)
	    e:SetText(WaaaghBarSettings.WList.width[WaaaghBarSettings.WList.name[i]])
	    --W.EditWidth = e
	    ins(TB, e)
    end
    
    W:Resize(500, y+170) -- 120 is the height of button
    
    -- Apply button
    e = W("Button")
    e:Resize(200)
    e:SetText(L"Apply")
    e:AnchorTo(W, "bottomleft", "bottomleft", 40, -20)
    e.OnLButtonUp = function() ApplyPanel() end
    W.ButtonApply = e
    
    -- Revert button
    e = W("Button")
    e:Resize(200)
    e:SetText(L"Revert")
    e:AnchorTo(W, "bottomright", "bottomright", -40, -20)
    e.OnLButtonUp = function() UpdatePanel() end
    W.ButtonRevert = e
    
    return W
end

function ApplyPanel()
    -- Apply width
    for i = 1, getn(TB) do
    	WaaaghBarSettings.WList.width[WaaaghBarSettings.WList.name[i]] = tonumber(TB[i]:GetText())
    end
    WaaaghBar.ArrangePlugins()
end

function UpdatePanel(self, key, value)
	for i = 1, getn(WaaaghBarSettings.WList.name) do
		LL[i]:SetText(string.sub(WaaaghBarSettings.WList.name[i], 10))
	   	TB[i]:SetText(WaaaghBarSettings.WList.width[WaaaghBarSettings.WList.name[i]])
    end
end

-- Actually add the panel
panel.create = CreatePanel
panel.update = UpdatePanel
panel.id = WaaaghBarConfigurator.AddPanel(panel)