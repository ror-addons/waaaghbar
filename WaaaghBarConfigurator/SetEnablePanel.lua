if not WaaaghBarConfigurator then return end
local LibGUI = LibStub("LibGUI")
local CreatePanel, UpdatePanel, ApplyPanel
local panel = {}

local ins, getn = table.insert, table.getn
local BAR_ONE = "WaaaghBarOne"
local BAR_TWO = "WaaaghBarTwo"

-- Our panel window
local W

local LL --Label List
local CB --Checkbox List

panel.title = L"1. SetEnable"

-- Create the panel
function CreatePanel(self)
    -- Do we somehow have a window still around? If so, we're done, that was easy.
    if W and W.name then EA_ChatWindow.Print(L"Got Already!") return W end
    
    W = LibGUI("Blackframe")
    --W:Resize(500, 250)

    LL = {}
    CB = {}
        
    local e
    
    -- Title label
    e = W("Label")
    e:Resize(480)
    e:AnchorTo(W, "top", "top", 0, 10)
    e:Font("font_clear_medium_bold")
    e:SetText(L"SetEnable Options")
    W.Title = e
    
    local y = 70
    local name 
    for i = 1, getn(WaaaghBarSettings.PList.name) do
    	name = string.sub(WaaaghBarSettings.PList.name[i], 10)
    	y = y + 40
        -- Plugin Label
        e = W("Label")
    	e:Resize(180)
    	e:Position(60,y)
    	e:Align("rightcenter")
    	e:SetText(name)
    	ins(LL, e)
    
	    -- Plugin Checkbox to Hide
	    e = W("Checkbox")
	    e:AnchorTo(LL[i], "right", "left", 40, 0)
	    ins(CB, e)    
    end
    
    -- Display Msg
    e = W("Label")
    e:Resize(500)
    e:AnchorTo(W, "bottomleft", "bottomleft", 0, -80)
    e:Align("center")
    e:SetText("Warning!!! Apply will force interface to reload!")
    W.Msg = e
    
    W:Resize(500, y+180) -- 120 is the height of button
    
    -- Apply button
    e = W("Button")
    e:Resize(200)
    e:SetText(L"Apply")
    e:AnchorTo(W, "bottomleft", "bottomleft", 40, -20)
    e.OnLButtonUp = function() ApplyPanel() end
    W.ButtonApply = e
    
    -- Revert button
    e = W("Button")
    e:Resize(200)
    e:SetText(L"Revert")
    e:AnchorTo(W, "bottomright", "bottomright", -40, -20)
    e.OnLButtonUp = function() UpdatePanel() end
    W.ButtonRevert = e
    
    return W
end

function ApplyPanel()
	WaaaghBarSettings[BAR_ONE].OList.name = {}
	WaaaghBarSettings[BAR_TWO].OList.name = {}
	WaaaghBarSettings[BAR_ONE].OList.alignment ={}
	WaaaghBarSettings[BAR_TWO].OList.alignment ={}
		
	for i = 1, getn(CB) do
		WaaaghBarSettings.PList.show[WaaaghBarSettings.PList.name[i]] = CB[i]:GetValue()
		if CB[i]:GetValue() then
			ins(WaaaghBarSettings[BAR_ONE].OList.name, WaaaghBarSettings.PList.name[i])
			WaaaghBarSettings[BAR_ONE].OList.alignment[WaaaghBarSettings.PList.name[i]] = "center"
		end
		
--		WaaaghBarSettings[BAR_ONE].OList.alignment[WaaaghBarSettings.PList.name[i]] = "center"
--		if not CB[i]:GetValue() then
--			WaaaghBarSettings[BAR_ONE].OList.alignment[WaaaghBarSettings.PList.name[i]] = nil
--		end
	end
	WindowSetShowing(BAR_TWO, false)
	InterfaceCore.ReloadUI() 
end

function UpdatePanel(self, key, value)
	for i = 1, getn(CB) do
		if WaaaghBarSettings.PList.show[WaaaghBarSettings.PList.name[i]] then
			CB[i]:Check()
		else
			CB[i]:Clear()
		end
	end
end

-- Actually add the panel
panel.create = CreatePanel
panel.update = UpdatePanel
panel.id = WaaaghBarConfigurator.AddPanel(panel)