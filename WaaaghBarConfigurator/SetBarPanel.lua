if not WaaaghBarConfigurator then return end
local LibGUI = LibStub("LibGUI")
local CreatePanel, UpdatePanel, ApplyPanel, CheckInput
local panel = {}

local ins, getn = table.insert, table.getn
local BAR_ONE = "WaaaghBarOne"
local BAR_TWO = "WaaaghBarTwo"
local TOTAL_SHOW

-- Our panel window
local W

local LL --Label List
local CBONE --Checkbox List BAR_ONE
local CBTWO --Checkbox List BAR_TWO

panel.title = L"2. SetBars"

-- Create the panel
function CreatePanel(self)
    -- Do we somehow have a window still around? If so, we're done, that was easy.
    if W and W.name then EA_ChatWindow.Print(L"Got Already!") return W end
    
    W = LibGUI("Blackframe")
    --W:Resize(500, 250)

    LL = {}
    CBONE = {}
    CBTWO = {}
        
    local e
    
    -- Title label
    e = W("Label")
    e:Resize(480)
    e:AnchorTo(W, "top", "top", 0, 10)
    e:Font("font_clear_medium_bold")
    e:SetText(L"SetBars Options")
    W.Title = e
    
	e = W("Label")
    e:Resize(100)
    e:Position(240, 60)
    e:SetText(L"1")
    
    e = W("Label")
    e:Resize(100)
    e:Position(305, 60)
    e:SetText(L"2")
    
    local y = 70
    local name
--    local LLi = 1
    for i = 1, getn(WaaaghBarSettings.PList.name) do
--    	if WaaaghBarSettings.PList.show[WaaaghBarSettings.PList.name[i]] then
--	    	name = string.sub(WaaaghBarSettings.PList.name[i], 10)
    		y = y + 40
    	    -- Plugin Label
    	    e = W("Label")
	    	e:Resize(180)
   		 	e:Position(60,y)
   		 	e:Align("rightcenter")
--    		e:SetText(name)
    		ins(LL, e)
    		
		    -- Plugin Checkbox
		    e = W("Checkbox")
--		    e:AnchorTo(LL[LLi], "right", "left", 40, 0) -- Non Exist Index
			e:AnchorTo(LL[i], "right", "left", 40, 0)
		    e:RegisterEvent("OnLButtonUp")
		    ins(CBONE, e)
	    
		    e = W("Checkbox")
--		    e:AnchorTo(CBONE[LLi], "right", "left", 40, 0)
			e:AnchorTo(CBONE[i], "right", "left", 40, 0)
		    e:RegisterEvent("OnLButtonUp")
		    ins(CBTWO, e)
		    
		    CBONE[i].OnLButtonUp = function()
		    	CBTWO[i]:Clear()
		    end
		    
		    CBTWO[i].OnLButtonUp = function()
		    	CBONE[i]:Clear()
		    end
		    
--		    LLi = LLi + 1
--	    end
    end
    
    -- Display Msg
    e = W("Label")
    e:Resize(500)
    e:AnchorTo(W, "bottomleft", "bottomleft", 0, -80)
    e:Align("center")
    e:SetText("Set what you want to show on each bars.")
    W.Msg = e
    
--    W:Resize(500, y+180) -- 120 is the height of button
    
    -- Apply button
    e = W("Button")
    e:Resize(200)
    e:SetText(L"Apply")
    e:AnchorTo(W, "bottomleft", "bottomleft", 40, -20)
    e.OnLButtonUp = function() ApplyPanel() end
    W.ButtonApply = e
    
    -- Revert button
    e = W("Button")
    e:Resize(200)
    e:SetText(L"Revert")
    e:AnchorTo(W, "bottomright", "bottomright", -40, -20)
    e.OnLButtonUp = function() UpdatePanel() end
    W.ButtonRevert = e
    
    return W
end

function CheckInput()
	for i = 1, TOTAL_SHOW do
		if CBONE[i]:GetValue() == false and CBTWO[i]:GetValue() == false then
			return false
		end
	end
	return true
end

function ApplyPanel()
	if CheckInput() then
		WaaaghBarSettings[BAR_ONE].OList.name = {}
		WaaaghBarSettings[BAR_ONE].OList.alignment = {}
		WaaaghBarSettings[BAR_TWO].OList.name = {}
		WaaaghBarSettings[BAR_TWO].OList.alignment = {}
		local name
		for i = 1, getn(LL) do
			name = tostring(L"WaaaghBar"..LL[i]:GetText())
			if CBONE[i]:GetValue() then
				ins(WaaaghBarSettings[BAR_ONE].OList.name, name)
				WaaaghBarSettings[BAR_ONE].OList.alignment[name] = "center"
			end
			if CBTWO[i]:GetValue() then
				ins(WaaaghBarSettings[BAR_TWO].OList.name, name)
				WaaaghBarSettings[BAR_TWO].OList.alignment[name] = "center"
			end
		end

		if WaaaghBar.Stats.CheckLevel ~= nil then
			WaaaghBar.Stats.CheckLevel()
		else
			WaaaghBar.ArrangePlugins()
		end
		
		UpdatePanel()
		if getn(WaaaghBarSettings[BAR_ONE].OList.name) ~= 0 then
			WindowSetShowing	(BAR_ONE, true)
		else
			WindowSetShowing	(BAR_ONE, false)
		end
		if getn(WaaaghBarSettings[BAR_TWO].OList.name) ~= 0 then 
			WindowSetShowing	(BAR_TWO, true)
		else
			WindowSetShowing	(BAR_TWO, false)
		end
	else
		W.Msg:SetText("You must select a Bar for each plugin!")
	end
end

function UpdatePanel(self, key, value)
	local name
	local LLi = 1
	TOTAL_SHOW = getn(WaaaghBarSettings.PList.name)
	for i = 1, getn(WaaaghBarSettings.PList.name) do
		if WaaaghBarSettings.PList.show[WaaaghBarSettings.PList.name[i]] then
			name = WaaaghBarSettings.PList.name[i]
			LL[LLi]:SetText(string.sub(name, 10))
			if WaaaghBarSettings[BAR_ONE].OList.alignment[name] then
				CBONE[LLi]:Check()
			else
				CBONE[LLi]:Clear()
			end
			if WaaaghBarSettings[BAR_TWO].OList.alignment[name] then
				CBTWO[LLi]:Check()
			else
				CBTWO[LLi]:Clear()
			end
			LLi = LLi + 1
		else
			LL[TOTAL_SHOW]:Hide()
			CBONE[TOTAL_SHOW]:Hide()
			CBTWO[TOTAL_SHOW]:Hide()
			TOTAL_SHOW = TOTAL_SHOW - 1
		end
	end
	W:Resize(500, 70 + TOTAL_SHOW * 40 + 180)
	W.Msg:SetText("Set what you want to show on each bars.")
end

-- Actually add the panel
panel.create = CreatePanel
panel.update = UpdatePanel
panel.id = WaaaghBarConfigurator.AddPanel(panel)