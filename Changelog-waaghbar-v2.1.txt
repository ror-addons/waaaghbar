------------------------------------------------------------------------
r15 | shakakaw | 2008-10-08 05:58:51 +0000 (Wed, 08 Oct 2008) | 2 lines
Changed paths:
   A /tags/v2.1 (from /trunk:9)
   R /tags/v2.1/WaaaghBar.lua (from /trunk/WaaaghBar.lua:10)
   R /tags/v2.1/WaaaghBar.mod (from /trunk/WaaaghBar.mod:13)
   R /tags/v2.1/WaaaghBar.xml (from /trunk/WaaaghBar.xml:12)
   A /tags/v2.1/WaaaghBar_Location (from /trunk/WaaaghBar_Location:10)
   A /tags/v2.1/WaaaghBar_Money (from /trunk/WaaaghBar_Money:10)
   M /tags/v2.1/WaaaghBar_Money/WaaaghBar_Money.lua
   D /tags/v2.1/WaaaghBar_Money.lua
   A /tags/v2.1/WaaaghBar_Performance (from /trunk/WaaaghBar_Performance:10)
   M /tags/v2.1/WaaaghBar_Performance/WaaaghBar_Performance.lua
   A /tags/v2.1/WaaaghBar_Social (from /trunk/WaaaghBar_Social:10)
   M /tags/v2.1/WaaaghBar_Social/WaaaghBar_Social.lua
   A /tags/v2.1/WaaaghBar_Stats (from /trunk/WaaaghBar_Stats:10)
   A /tags/v2.1/WaaaghBar_System (from /trunk/WaaaghBar_System:10)
   M /tags/v2.1/WaaaghBar_System/WaaaghBar_System.lua
   M /tags/v2.1/WaaaghBar_System/WaaaghBar_System.mod

# Minor bug fixes
# Added slash compatability
------------------------------------------------------------------------
r9 | shakakaw | 2008-10-06 11:52:44 +0000 (Mon, 06 Oct 2008) | 2 lines
Changed paths:
   A /trunk/WaaaghBar.lua
   A /trunk/WaaaghBar.mod
   A /trunk/WaaaghBar.xml
   A /trunk/WaaaghBar_Money.lua
   D /trunk/WaaghBar.lua
   D /trunk/WaaghBar.mod
   D /trunk/WaaghBar.xml
   D /trunk/WaaghBar_Experience.lua
   D /trunk/WaaghBar_Experience.xml
   D /trunk/WaaghBar_Influence.lua
   D /trunk/WaaghBar_Money.lua
   D /trunk/WaaghBar_Renown.lua
   D /trunk/WaaghBar_Renown.xml

# Rewrite!
# Do Not Download This!
------------------------------------------------------------------------
r8 | shakakaw | 2008-10-06 02:09:02 +0000 (Mon, 06 Oct 2008) | 3 lines
Changed paths:
   M /trunk/WaaghBar.mod
   M /trunk/WaaghBar_Experience.lua
   A /trunk/WaaghBar_Experience.xml
   M /trunk/WaaghBar_Influence.lua
   M /trunk/WaaghBar_Renown.xml

# Added tooltip for Experience
# Fixed size of tooltip for Renown
# The Influence bar will now always show, it's up to the player to remove it
------------------------------------------------------------------------
r7 | shakakaw | 2008-10-05 17:27:03 +0000 (Sun, 05 Oct 2008) | 2 lines
Changed paths:
   M /trunk/WaaghBar.lua
   M /trunk/WaaghBar.mod
   M /trunk/WaaghBar.xml
   M /trunk/WaaghBar_Renown.lua
   A /trunk/WaaghBar_Renown.xml

# Completed Tooltip functionality
# Added tooltip for WaaaghBar_Renown
------------------------------------------------------------------------
r6 | shakakaw | 2008-10-05 11:29:34 +0000 (Sun, 05 Oct 2008) | 2 lines
Changed paths:
   M /trunk/WaaghBar.lua
   M /trunk/WaaghBar.mod
   M /trunk/WaaghBar.xml

# Added tooltip functionality (very basic)
# Added contxtmenhu functionality (very basic)
------------------------------------------------------------------------
r5 | shakakaw | 2008-10-04 14:00:08 +0000 (Sat, 04 Oct 2008) | 2 lines
Changed paths:
   M /trunk/WaaghBar.lua
   M /trunk/WaaghBar.mod
   M /trunk/WaaghBar.xml
   M /trunk/WaaghBar_Influence.lua

# Fixed bugs with WaaaghBar Influence plugin.
# Added the first sign of WaaghBar Menu!
------------------------------------------------------------------------
r4 | pwerelds | 2008-10-04 11:02:00 +0000 (Sat, 04 Oct 2008) | 1 line
Changed paths:
   M /trunk/WaaghBar_Influence.lua

Anchors for influencebar/PQ stuff are now copied dynamically instead of hardcoded ;)
------------------------------------------------------------------------
r3 | pwerelds | 2008-10-04 10:04:12 +0000 (Sat, 04 Oct 2008) | 1 line
Changed paths:
   M /trunk/WaaghBar_Influence.lua

Hopefully fixed the PQTracker temporarily, *Hints at Cronium*
------------------------------------------------------------------------
r2 | shakakaw | 2008-10-03 03:08:54 +0000 (Fri, 03 Oct 2008) | 8 lines
Changed paths:
   A /trunk/WaaghBar.lua
   A /trunk/WaaghBar.mod
   A /trunk/WaaghBar.xml
   A /trunk/WaaghBar_Experience.lua
   A /trunk/WaaghBar_Influence.lua
   A /trunk/WaaghBar_Money.lua
   A /trunk/WaaghBar_Renown.lua

# Basic WaaghBar functionality has been added
# The first plugins have been added.
## Money
## Experience
## Renown
## Influence

Please notice that alot of changes are incomming. This is the first beta release of WaaghBar and it has not been in development for long. Have patience, instead of reporting what seems like bugs, you should come with suggestions, or even start writing your own plugins now!
------------------------------------------------------------------------
r1 | root | 2008-10-02 18:37:48 +0000 (Thu, 02 Oct 2008) | 1 line
Changed paths:
   A /branches
   A /tags
   A /trunk

"waaghbar/mainline: Initial Import"
------------------------------------------------------------------------
