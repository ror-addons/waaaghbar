-- File: WaaaghBar_BagSpace.lua
-- Version: 0.4
-- Author: Ben Logan (InfectiousX)
-- Date: 16/10/08
-- Email: roflatroadkill@gmail.com

-- Fixed for 1.3 by Martin Midtgaard (martin.midtgaard@gmail.com)

local BagSpace = {}

local totalSpaceGeneral
local availSpaceGeneral
local totalSpaceCurrency
local availSpaceCurrency
local totalSpaceCrafting
local availSpaceCrafting
local questItems

function BagSpace.Initialize()
	BagSpace.Waaagh = WaaaghBar.NewPlugin("WaaaghBarBagSpace")
	--BagSpace.BagSpace = BagSpace.Waaagh:NewElement("BagSpace", 250, L"BagSpace", { r = 255, g = 255, b = 255 }, 00048, { abr = "bagspace", func = BagSpace.OnBagSlash })
	BagSpace.BagSpace = BagSpace.Waaagh:NewElement("BagSpace", 250, L"BagSpace", { r = 255, g = 255, b = 255 }, 00061, { abr = "bagspace", func = BagSpace.OnBagSlash })
	BagSpace.BagSpace:SetIconScale(0.8)
	if not WaaaghBarBagSpaceSettings then
        WaaaghBarBagSpaceSettings = {
            showQuest = true,
			showAvail = false
        }
    end
	-- Register with Events
	--RegisterEventHandler(SystemData.Events.PLAYER_BAGSPACE_UPDATED, "WaaaghBar.BagSpace.OnPlayerBagSpaceUpdated")
	RegisterEventHandler(SystemData.Events.PLAYER_INVENTORY_SLOT_UPDATED, "WaaaghBar.BagSpace.OnPlayerBagSpaceUpdated")
	RegisterEventHandler(SystemData.Events.PLAYER_QUEST_ITEM_SLOT_UPDATED, "WaaaghBar.BagSpace.OnPlayerBagSpaceUpdated")
	RegisterEventHandler(SystemData.Events.PLAYER_CAREER_RANK_UPDATED, "WaaaghBar.BagSpace.GetPlayerLevel")
	WindowRegisterCoreEventHandler(BagSpace.BagSpace.name, "OnMouseOver", "WaaaghBar.BagSpace.OnMouseOver")
	WindowRegisterCoreEventHandler(BagSpace.BagSpace.name, "OnLButtonDown", "WaaaghBar.BagSpace.OnLButtonDown")
	BagSpace.OnPlayerBagSpaceUpdated()
end

function BagSpace.SetDefaultSettings()
	WaaaghBarBagSpaceSettings = {
            showQuest = true,
			showAvail = false
	}
	BagSpace.OnPlayerBagSpaceUpdated()
end

function BagSpace.OnLButtonDown()
	if WindowGetShowing("EA_Window_Backpack") == true then
		WindowSetShowing("EA_Window_Backpack", false)
	else
		WindowSetShowing("EA_Window_Backpack", true)
	end
end

function BagSpace.OnBagSlash(opt, val)
	if	   opt == "showquest" then
		if	   val == "false" then
			WaaaghBarBagSpaceSettings.showQuest = false
		elseif val == "true" then
			WaaaghBarBagSpaceSettings.showQuest = true
		end
	elseif opt == "showavail" then
		if val == "true" then
			WaaaghBarBagSpaceSettings.showAvail = true
		elseif val == "false" then
			WaaaghBarBagSpaceSettings.showAvail = false
		end
	end
	BagSpace.OnPlayerBagSpaceUpdated()
end

function BagSpace.OnPlayerBagSpaceUpdated()

	BagSpace.PhaseMagic()
end

function BagSpace.GetPlayerLevel()
	playerLevel =  GameData.Player.level
end

function BagSpace.PhaseMagic()
	local pQuests = EA_Window_Backpack.GetNumberOfItemsInPocket(1) --quest items
	local pGeneral = EA_Window_Backpack.GetNumberOfItemsInPocket(2) --general items
	local pCurrency = EA_Window_Backpack.GetNumberOfItemsInPocket(3) --currency items
	local pCrafting = EA_Window_Backpack.GetNumberOfItemsInPocket(4) --crafting items
	local pMax = EA_Window_Backpack.GetMaxSizeForPocket(2) --general items
	
	totalSpaceGeneral = EA_Window_Backpack.GetMaxSizeForPocket(2)
	availSpaceGeneral = totalSpaceGeneral - pGeneral
	totalSpaceCurrency = EA_Window_Backpack.GetMaxSizeForPocket(3)
	availSpaceCurrency = totalSpaceCurrency - pCurrency
	totalSpaceCrafting = EA_Window_Backpack.GetMaxSizeForPocket(4)
	availSpaceCrafting = totalSpaceCrafting - pCrafting
	questItems = pQuests
	
	if WaaaghBarBagSpaceSettings.showAvail == true then
		pInfo = pMax - pGeneral
	else
		pInfo = pGeneral
	end
	if WaaaghBarBagSpaceSettings.showQuest == true then
		BagSpace.BagSpace:SetText(towstring(string.format("%i / %i (%i Quest Items)", pInfo, pMax, pQuests)))
	else
		BagSpace.BagSpace:SetText(towstring(string.format("%i / %i", pInfo, pMax)))
	end
end

function BagSpace.OnMouseOver()
    Tooltips.CreateTextOnlyTooltip( WaaaghBar.BagSpace.BagSpace.name, nil)
    Tooltips.AnchorTooltip( WaaaghBar.GetTooltipsAnchor() )
    Tooltips.SetTooltipColor( 1, 1, 255, 140, 0 )
    Tooltips.SetTooltipText( 1, 1, L"BAGSPACE")
    Tooltips.SetTooltipText( 2, 1, L"Available Slots:" )
	Tooltips.SetTooltipText( 2, 2, towstring(string.format("%5d", availSpaceGeneral)) )
	Tooltips.SetTooltipText( 3, 1, L"Total Slots:" )
	Tooltips.SetTooltipText( 3, 2, towstring(string.format("%5d", totalSpaceGeneral)) )
	Tooltips.SetTooltipText( 4, 1, L"Quest Items:" )
	Tooltips.SetTooltipText( 4, 2, towstring(string.format("%5d", questItems)) )
	Tooltips.SetTooltipText( 5, 1, L"Currency Slots:" )
	Tooltips.SetTooltipText( 5, 2, towstring(string.format("%5d", availSpaceCurrency)) )
	Tooltips.SetTooltipText( 6, 1, L"Crafting Slots:" )
	Tooltips.SetTooltipText( 6, 2, towstring(string.format("%5d", availSpaceCrafting)) )
	Tooltips.Finalize()
end

function EA_Window_Backpack.GetNumberOfItemsInPocket( pocketNumber )

	local items = EA_Window_Backpack.GetItemsFromBackpack(pocketNumber)
	local count = 0
	for _, item in ipairs(items) do 
		if EA_Window_Backpack.ValidItem(item) then
			count = count + 1
		end
	end
	
	return count
end

function EA_Window_Backpack.GetMaxSizeForPocket(pocketNumber)

	return EA_Window_Backpack.numberOfSlots[pocketNumber]
end

WaaaghBar.BagSpace = BagSpace
