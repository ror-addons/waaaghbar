<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">	
  <UiMod name="WaaaghBar_BagSpace" version="0.4" date="17/10/2008">		
    <VersionSettings gameVersion="1.9.9" windowsVersion="1.0" savedVariablesVersion="1.0" />		
    <Author name="Ben Logan" email="roflatroadkill@gmail.com" />		
    <Description text="http://war.curseforge.com/projects/wb_bagspace/" />	   		
    <Dependencies>			
      <Dependency name="WaaaghBar" />			
      <Dependency name="EA_BackpackWindow" />		
    </Dependencies>          
    <SavedVariables>            
      <SavedVariable name="WaaaghBarBagSpaceSettings" />        
    </SavedVariables>		
    <Files>			
      <File name="WaaaghBar_BagSpace.lua" />		
    </Files>		
    <OnInitialize>			
      <CallFunction name="WaaaghBar.BagSpace.Initialize" />		
    </OnInitialize>	
  </UiMod>
</ModuleFile>