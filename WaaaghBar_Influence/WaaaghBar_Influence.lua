-- WaaaghBar module for Influence Text display, based off an older version of WaaaghBar_Influence by Shakakaw
-- WaaaghBar can be found here: http://war.curse.com/downloads/war-addons/details/waaaghbar.aspx
-- version 0.3
-- author: Oscarr <oscarr_d24@yahoo.com>
-- Feel free to use this code for anything you wish. (Public Domain)

local WInfluence = {}
local rewardlevel = 3

function WInfluence.Initialize()
	if not (type(WaaaghBarWInfluenceSettings)=="table") then
		WaaaghBarWInfluenceSettings = {
			toggle_bar = 1,
		}
	end

	WInfluence.Waaagh = WaaaghBar.NewPlugin("WaaaghBarInf")
	WInfluence.DisplayInf = WInfluence.Waaagh:NewElement("DisplayInf", 300, L"Influence", { r = 64, g = 224, b = 208 }, 00112 )
	WInfluence.DisplayInf:SetText(towstring("Influence"))
	-- Register Player Influence Update Event / Mouse Events
	RegisterEventHandler(SystemData.Events.PLAYER_INFLUENCE_UPDATED, "WaaaghBar.WInfluence.PlayerInfUpdated")
	WindowRegisterCoreEventHandler(WInfluence.DisplayInf.name, "OnMouseOver", "WaaaghBar.WInfluence.OnMouseOver")
	WindowRegisterCoreEventHandler(WInfluence.Waaagh.name, "OnLButtonDown", "WaaaghBar.WInfluence.OnLButtonDown")
	WindowRegisterCoreEventHandler(WInfluence.Waaagh.name, "OnRButtonDown", "WaaaghBar.WInfluence.OnRButtonDown")
	WindowRegisterCoreEventHandler(WInfluence.Waaagh.name, "OnMButtonDown", "WaaaghBar.WInfluence.OnMButtonDown")
	EA_ChatWindow.Print(L"Influence WaaaghBar Module Loaded")
	WInfluence.PlayerInfUpdated()
	if WaaaghBarWInfluenceSettings.toggle_bar == 2 then
		WInfluence.ToggleInfluenceBar()
	end
end

function WInfluence.SetDefaultSettings()
	WaaaghBarWInfluenceSettings = {
			toggle_bar = 1,
	}
	WInfluence.PlayerInfUpdated()
	WindowSetShowing("EA_Window_PublicQuestTrackerInfluenceBar", true)
	WindowSetShowing("EA_Window_PublicQuestTrackerLocationPublicQuestIcon", true)
	WindowSetShowing("EA_Window_PublicQuestTrackerLocationPairingLabel", true)
	WindowSetShowing("EA_Window_PublicQuestTrackerLocationChapterLabel", true)
end

function WInfluence.ToggleInfluenceBar()
	EA_ChatWindow.Print(L"Toggling Default Influence Bar")
	WindowUtils.ToggleShowing("EA_Window_PublicQuestTrackerInfluenceBar")
	WindowUtils.ToggleShowing("EA_Window_PublicQuestTrackerLocationPublicQuestIcon")
	WindowUtils.ToggleShowing("EA_Window_PublicQuestTrackerLocationPairingLabel")
	WindowUtils.ToggleShowing("EA_Window_PublicQuestTrackerLocationChapterLabel")
end

function WInfluence.OnMButtonDown()
	if WaaaghBarWInfluenceSettings.toggle_bar == 2 then
		WaaaghBarWInfluenceSettings.toggle_bar = 0
	end
	WaaaghBarWInfluenceSettings.toggle_bar = WaaaghBarWInfluenceSettings.toggle_bar + 1
	WInfluence.ToggleInfluenceBar()
end

function WInfluence.OnLButtonDown()
	local influenceId = WInfluence.GetLocalAreaInfluenceID()
    local influenceData = DataUtils.GetInfluenceData(influenceId)
	if WindowGetShowing("TomeWindow") == true then
		WindowSetShowing("TomeWindow", false)
	else
		if influenceData.tomeSection ~= nil then
			TomeWindow.OpenTomeToEntry( influenceData.tomeSection, influenceData.tomeEntry )
		else
			TomeWindow.OpenTomeToEntry(0,0)
		end
	end
end

function WInfluence.OnRButtonDown()
	if rewardlevel == 3 then
		rewardlevel = 0
	end
	rewardlevel = rewardlevel + 1
	WInfluence.PlayerInfUpdated() -- Call this to update the influence displayed
	WInfluence.OnMouseOver() -- Call this again to update the tooltip, which will be showing
end

function WInfluence.OnMouseOver()
	local influenceId = WInfluence.GetLocalAreaInfluenceID()
	local raceLocation = StringUtils.GetFriendlyRaceForCurrentPairing( zonePairing, true )
	local chapterNum = GetChapterShortName(influenceId)
	

    Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil)
    
	Tooltips.AnchorTooltip( WaaaghBar.GetTooltipsAnchor() )

    Tooltips.SetTooltipColor( 1, 1, 64, 224, 208 )
    Tooltips.SetTooltipText( 1, 1, towstring(string.format("%s (%s): %s %i", tostring(raceLocation), tostring(chapterNum), tostring(L"Reward Level: "), tostring(rewardlevel) )))
	Tooltips.SetTooltipText( 2, 1, L"Left Click to open to Tome Entry" )
	Tooltips.SetTooltipText( 3, 1, L"Middle Click to hide the default Influence Bar" )
	Tooltips.SetTooltipText( 4, 1, L"Right Click to toggle Reward Level" )
	Tooltips.Finalize()
end

function WInfluence.PlayerInfUpdated()
	local influenceId = WInfluence.GetLocalAreaInfluenceID()
    local influenceData = DataUtils.GetInfluenceData(influenceId)
    if(influenceData == nil) then
		return false
    end
	local raceLocation = StringUtils.GetFriendlyRaceForCurrentPairing( zonePairing, true )
	local chapterNum = GetChapterShortName(influenceId)
	local influenceEarned = influenceData.curValue
	local influenceNeeded1 = influenceData.rewardLevel[1].amountNeeded 
	local influenceNeeded2 = influenceData.rewardLevel[2].amountNeeded 
	local influenceNeeded3 = influenceData.rewardLevel[3].amountNeeded
	if rewardlevel == 3 then
		local influencePercent = influenceEarned / influenceNeeded3 * 100
		WInfluence.DisplayInf:SetText(towstring(string.format("%s %i / %i (%.2f%%)", tostring("L3: "), influenceEarned, influenceNeeded3, influencePercent)))
	end
	if rewardlevel == 2 then
		if influenceEarned > influenceNeeded2 then influenceEarned = influenceNeeded2 end
		local influencePercent = influenceEarned / influenceNeeded2 * 100
		WInfluence.DisplayInf:SetText(towstring(string.format("%s %i / %i (%.2f%%)", tostring("L2: "), influenceEarned, influenceNeeded2, influencePercent)))
	end
	if rewardlevel == 1 then
		if influenceEarned > influenceNeeded1 then influenceEarned = influenceNeeded1 end
		local influencePercent = influenceEarned / influenceNeeded1 * 100
		WInfluence.DisplayInf:SetText(towstring(string.format("%s %i / %i (%.2f%%)", tostring("L1: "), influenceEarned, influenceNeeded1, influencePercent)))
	end
end

-- these function is ripped from DataUtils
function WInfluence.GetLocalAreaInfluenceID()
    local areaData = GetAreaData()
    
    if( areaData == nil )
    then
        -- DEBUG(L"[EA_Window_PublicQuestTracker.GetLocalAreaInfluenceID] AreaData returned nil")
        return nil
    end
    
    for key, value in ipairs( areaData )
    do
        -- These should match the data that was retrived from war_interface::LuaGetAreaData
        if (value.influenceID ~= 0)
        then
            return value.influenceID
        end
    end
    
    return nil
end

WaaaghBar.WInfluence = WInfluence

