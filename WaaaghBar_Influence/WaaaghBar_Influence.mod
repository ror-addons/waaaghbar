<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">	
  <UiMod name="WaaaghBar_Influence" version="0.3" date="19/10/2008">		
    <VersionSettings gameVersion="1.9.9" windowsVersion="1.0" savedVariablesVersion="1.0" />		
    <Author name="Oscarr" email="oscarr_d24@yahoo.com" />		
    <Description text="WaaaghBar Module for displaying Influence" />		
    <Dependencies>			
      <Dependency name="WaaaghBar" />		
    </Dependencies>   		
    <Files>			
      <File name="WaaaghBar_Influence.lua" />		
    </Files>		
    <OnInitialize>			
      <CallFunction name="WaaaghBar.WInfluence.Initialize" />		
    </OnInitialize>		
    <SavedVariables>            
      <SavedVariable name="WaaaghBarWInfluenceSettings" />        
    </SavedVariables>		 	
  </UiMod>
</ModuleFile>