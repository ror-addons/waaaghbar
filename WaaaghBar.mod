<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">	
  <UiMod name="WaaaghBar" version="2.1" date="08/10/2008">		
    <VersionSettings gameVersion="1.9.9" windowsVersion="1.0" savedVariablesVersion="1.0" />		
    <Author name="Hallas" email="warui@hsdev.eu" />		
    <Description text="http://war.curseforge.com/projects/waaaghbar/" />	   		
    <Dependencies>			
      <Dependency name="LibSlash" />		
    </Dependencies>		
    <SavedVariables>            
      <SavedVariable name="WaaaghBarSettings" />        
    </SavedVariables>		 		
    <Files>			
      <File name="WaaaghBar.xml" />			
      <File name="WaaaghBarSettings.lua" />		
    </Files>		
    <OnInitialize>			
      <CreateWindow name="WaaaghBarOne" show="true" />			
      <CreateWindow name="WaaaghBarTwo" show="true" />			
      <CallFunction name="WaaaghBar.Initialize" />		
    </OnInitialize>	
  </UiMod>
</ModuleFile>