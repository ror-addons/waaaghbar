WaaaghBar = {
	plugins = {},
	slashHandlers = {},
}

local ins, getn = table.insert, table.getn
local BAR_ONE = "WaaaghBarOne"
local BAR_TWO = "WaaaghBarTwo"

function WaaaghBar.Initialize()
	if not (type(WaaaghBarSettings)=="table") or (WaaaghBarSettings.version < 2.4) then
		WaaaghBarSettingsSetDefaultTable()
		EA_ChatWindow.Print(L"WaaaghBar setting default Table 2.4")
		DialogManager.MakeOneButtonDialog(
            L"We now support 2 Bars, however, if you like 1 Bar just simply don't select anything on one of the Bar.",
            L"Ok", nil)
	end
	
	WindowSetTintColor	(BAR_ONE.."Background", 0, 0, 0)
	WindowSetAlpha		(BAR_ONE.."Background", WaaaghBarSettings[BAR_ONE].opacity)
	if getn(WaaaghBarSettings[BAR_ONE].OList.name) == 0 then
		WindowSetShowing	(BAR_ONE, false)
	else
		WindowSetShowing	(BAR_TWO, true)
	end
	
	WindowSetTintColor	(BAR_TWO.."Background", 0, 0, 0)
	WindowSetAlpha		(BAR_TWO.."Background", WaaaghBarSettings[BAR_TWO].opacity)
	if getn(WaaaghBarSettings[BAR_TWO].OList.name) == 0 then
		WindowSetShowing	(BAR_TWO, false)
	else
		WindowSetShowing	(BAR_TWO, true)
	end

	RegisterEventHandler(SystemData.Events.LOADING_END, "WaaaghBar.ArrangePlugins")
	RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "WaaaghBar.ArrangePlugins")
	-- SLASH
	LibSlash.RegisterSlashCmd("wgb", function(cmd) WaaaghBar.SlashHandler(cmd) end)
	WaaaghBar.slashHandlers["main"] = WaaaghBar.OnMain
	WaaaghBar.slashHandlers["setwidth"] = WaaaghBar.OnSetWidth
end

function WaaaghBar.SlashHandler(cmd)
	local tar, opt, val = cmd:match("([a-zA-Z0-9]+)[ ]([a-zA-Z0-9]+)[ ]?(.*)")
	if WaaaghBar.slashHandlers[tar] then
		WaaaghBar.slashHandlers[tar](opt, val)
	else
		print("Opening WaaaghBar GUI...")
		WaaaghBarConfigurator.Show()
	end
end

function WaaaghBar.OnMain(opt, val)
	if opt == "lock" then
		if WindowGetMovable(BAR_ONE) then
			WindowSetMovable(BAR_ONE, false)
			WindowSetMovable(BAR_TWO, false)
		else
			WindowSetMovable(BAR_ONE, true)
			WindowSetMovable(BAR_TWO, true)
		end
	elseif opt == "showlist" then
		EA_ChatWindow.Print( L"Printing List..." )
		for i = 1, getn(WaaaghBar.plugins) do
				TextLogAddEntry("Chat", 3 , L"Plugin: "..towstring(WaaaghBar.plugins[i].name))
			for j = 1, getn(WaaaghBar.plugins[i].elements) do
				local name = towstring(WaaaghBar.plugins[i].elements[j].name)
				local width = towstring(WaaaghBar.plugins[i].elements[j].width)
				TextLogAddEntry("Chat", 5 , L"       Element: "..name..L" "..width)			
			end
		end
	end
end

function WaaaghBar.OnSetWidth(val1, val2) 
	for i = 1, getn(WaaaghBar.plugins) do
		for j = 1, getn(WaaaghBar.plugins[i].elements) do			
			if (WaaaghBar.plugins[i].elements[j].name == val1) then
			WaaaghBar.plugins[i].elements[j]:SetSize(tonumber(val2))
			WaaaghBar.SavePlugins()
			TextLogAddEntry("Chat", 4 , towstring(WaaaghBar.plugins[i].name)..L" now has width "..towstring(val2))
			end
		end
	end
	WaaaghBar.ArrangePlugins()
end

function WaaaghBar.SetBarFont(bar, font)
	if bar == BAR_ONE then
		for i = 1, getn(WaaaghBarSettings[BAR_ONE].OList.name) do
			for j = 1, getn(WaaaghBar.plugins[i].elements) do			
				WaaaghBar.plugins[i].elements[j]:SetFont(font)
			end
		end
	elseif bar == BAR_TWO then
		local TOTAL_E = getn(WaaaghBarSettings[BAR_ONE].OList.name) + getn(WaaaghBarSettings[BAR_TWO].OList.name)
		for i = getn(WaaaghBarSettings[BAR_ONE].OList.name) + 1, TOTAL_E do
			for j = 1, getn(WaaaghBar.plugins[i].elements) do			
				WaaaghBar.plugins[i].elements[j]:SetFont(font)
			end
		end
	end
end

function WaaaghBar.GetTooltipsAnchor()
	local y = SystemData.MousePosition.y
	local anchor = Tooltips.ANCHOR_WINDOW_TOP
	if ( y < 300 ) then
		anchor = Tooltips.ANCHOR_WINDOW_BOTTOM
	end
	return anchor		
end

function WaaaghBar.NewPlugin(name, slash)
	local plugin = {	
		name = name,
		elements = {},
	}

	ins(WaaaghBar.plugins, plugin)
	WaaaghBarPlugin:New(plugin.name)
	setmetatable(plugin, { __index = WaaaghBar.plugin })
	WindowSetParent(plugin.name, tostring(BAR_ONE))
	return plugin
end

function WaaaghBar.GetElement(name)
	for i = 1, getn(WaaaghBar.plugins) do 	
		for j = 1, getn(WaaaghBar.plugins[i].elements) do
			if WaaaghBar.plugins[i].elements[j].name == name then
				return WaaaghBar.plugins[i].elements[j]
			end
		end
	end
	return false
end

function WaaaghBar.GetPlugin(name)
	for i = 1, getn(WaaaghBar.plugins) do 	
		if WaaaghBar.plugins[i].name == name then
			return WaaaghBar.plugins[i]
		end
	end
	return false
end

function WaaaghBar.RemovePlugin(name) -- Temp Remove from Plugins List
	for i = 1, getn(WaaaghBar.plugins) do
		if WaaaghBar.plugins[i].name == name then
			WindowClearAnchors(name)
			WindowSetShowing(name, false)
			table.remove(WaaaghBar.plugins, i)
			return true
		end
	end
end

function WaaaghBar.DestroyPlugin(name) -- Destroy to Release all Resource
	for i = 1, getn(WaaaghBar.plugins) do
		if WaaaghBar.plugins[i].name == name then
			WindowClearAnchors(name)
			WindowSetShowing(name, false)
			WaaaghBar.plugins[i].name = nil
			WaaaghBar.plugins[i].elements = nil
			table.remove(WaaaghBar.plugins, i)
			return true
		end
	end
end

------------------------------
--Custom Organization Function
------------------------------
function WaaaghBar.MatchPlugins() -- To match the actual installed plugins with the Master List
	for i = 1, getn(WaaaghBar.plugins) do
		if WaaaghBarSettings.PList.show[WaaaghBar.plugins[i].name] == nil then
			ins(WaaaghBarSettings.PList.name, WaaaghBar.plugins[i].name)
			WaaaghBarSettings.PList.show[WaaaghBar.plugins[i].name] = true
		end
	end
end

function WaaaghBar.OrderPlugins() -- To Sort the Plugins List to BAR_ONE, BAR_TWO, and new plugins to the end
	local neworder = {}
	-- Add BAR_ONE plugins to neworder
	for i = 1, getn(WaaaghBarSettings[BAR_ONE].OList.name) do
		if WaaaghBar.GetPlugin(WaaaghBarSettings[BAR_ONE].OList.name[i]) then
			ins(neworder, WaaaghBar.GetPlugin(WaaaghBarSettings[BAR_ONE].OList.name[i]))
			WaaaghBar.RemovePlugin(WaaaghBarSettings[BAR_ONE].OList.name[i])
		end
		WindowSetParent(WaaaghBarSettings[BAR_ONE].OList.name[i], BAR_ONE)
	end
	-- Add BAR_TWO plugins to neworder
	for i = 1, getn(WaaaghBarSettings[BAR_TWO].OList.name) do
		if WaaaghBar.GetPlugin(WaaaghBarSettings[BAR_TWO].OList.name[i]) then
			ins(neworder, WaaaghBar.GetPlugin(WaaaghBarSettings[BAR_TWO].OList.name[i]))
			WaaaghBar.RemovePlugin(WaaaghBarSettings[BAR_TWO].OList.name[i])
		end
		WindowSetParent(WaaaghBarSettings[BAR_TWO].OList.name[i], BAR_TWO)
	end
	for i = 1, getn(WaaaghBar.plugins) do 	-- Add back the new plugins to the end of neworder
		ins(neworder, WaaaghBar.plugins[i])
		ins(WaaaghBarSettings[BAR_TWO].OList.name, WaaaghBar.plugins[i].name)
		WaaaghBarSettings[BAR_TWO].OList.alignment[WaaaghBar.plugins[i].name] = "right"
		WindowSetShowing	(BAR_TWO, true)
		for j = 1, getn(WaaaghBar.plugins[i].elements) do
			ins(WaaaghBarSettings.WList.name, WaaaghBar.plugins[i].elements[j].name)
			WaaaghBarSettings.WList.width[WaaaghBar.plugins[i].elements[j].name] = WaaaghBar.plugins[i].elements[j]:GetSize()
		end
		WindowSetParent(WaaaghBarSettings[BAR_TWO].OList.name[i], BAR_TWO)
	end
	return neworder
end

-------------------------------
--Rearrange the WList
-------------------------------
function WaaaghBar.SavePlugins()
	local namelist = {}
	local widthlist = {}
	
	for i = 1, getn(WaaaghBar.plugins) do -- Set the WaaaghBarSettings.WList as to the current enable plugins and not include all disable plugins
		for j = 1, getn(WaaaghBar.plugins[i].elements) do
			local elementname = WaaaghBar.plugins[i].elements[j].name
			local elementwidth = WaaaghBar.plugins[i].elements[j]:GetSize()
			if elementwidth == 0 then
				ins(namelist, elementname)
				widthlist[elementname] = WaaaghBarSettings.WList.width[elementname]
			else
				ins(namelist, elementname)
				widthlist[elementname] = elementwidth
			end
		end
	end
	
	WaaaghBarSettings.WList.name = namelist
	WaaaghBarSettings.WList.width = widthlist
end

function WaaaghBar.ArrangePlugins()
	local one_leftwidth, two_leftwidth = 0, 0
	local one_centerwidth, two_centerwidth = 0, 0
	local one_rightwidth, two_rightwidth = 0, 0
	local one_ci, two_ci = 0, 0
	local one_ri, two_ri = 0, 0

	WaaaghBar.MatchPlugins()
	
	for i = 1, getn(WaaaghBarSettings.PList.name) do	-- Code to remove selected plugins
		if  not WaaaghBarSettings.PList.show[WaaaghBarSettings.PList.name[i]] then
			WaaaghBar.DestroyPlugin(WaaaghBarSettings.PList.name[i])
		end
	end													-- END Code to remove selected plugins
					
	for i = 1, getn(WaaaghBarSettings.WList.name) do	-- Code to set preferred width
		if WaaaghBar.GetElement(WaaaghBarSettings.WList.name[i]) then
			if WindowGetShowing(WaaaghBarSettings.WList.name[i]) then
				WaaaghBar.GetElement(WaaaghBarSettings.WList.name[i]):SetSize(WaaaghBarSettings.WList.width[WaaaghBarSettings.WList.name[i]])
			else
				WaaaghBar.GetElement(WaaaghBarSettings.WList.name[i]):SetSize(0)
			end
		end
	end													-- Code to set preferred width					

	WaaaghBar.plugins = WaaaghBar.OrderPlugins() 

	for i = 1, getn(WaaaghBar.plugins) do	
		local elementoffsetX = 0 			-- New Code: Resetting Anchors for Elements so that it's space correctly
		for j = 2, getn(WaaaghBar.plugins[i].elements) do
			local elementname = WaaaghBar.plugins[i].elements[j].name
			WindowClearAnchors(elementname)
			elementoffsetX = elementoffsetX + WaaaghBar.plugins[i].elements[j-1]:GetSize()
			WindowAddAnchor(elementname, "left", WaaaghBar.plugins[i].name, "left", elementoffsetX, 0)
			WindowSetShowing(elementname, true)
		end 								-- END New Code: Resetting Anchors for Elements so that it's space correctly
		local name = WaaaghBar.plugins[i].name
		
		-- Set BAR_ONE
		if WaaaghBarSettings[BAR_ONE].OList.alignment[name] == "left" then
			WindowClearAnchors(name)
			WindowAddAnchor(name, "left", BAR_ONE, "left", one_leftwidth, 0)
			WindowSetShowing(name, true)
			one_leftwidth = one_leftwidth + WaaaghBar.plugins[i]:GetSize()
		elseif WaaaghBarSettings[BAR_ONE].OList.alignment[name] == "right" then -- Figure out the total width on right alignment and set the starting index
			one_rightwidth = one_rightwidth - WaaaghBar.plugins[i]:GetSize()
			if one_ri == 0 then	one_ri = i end
		elseif WaaaghBarSettings[BAR_ONE].OList.alignment[name] =="center" then -- Figure out the total width on center alignment and set the starting index
			one_centerwidth = one_centerwidth + WaaaghBar.plugins[i]:GetSize()
			if one_ci == 0 then one_ci = i end
		end
		
		-- Set BAR_TWO
		if WaaaghBarSettings[BAR_TWO].OList.alignment[name] == "left" then
			WindowClearAnchors(name)
			WindowAddAnchor(name, "left", BAR_TWO, "left", two_leftwidth, 0)
			WindowSetShowing(name, true)			
			two_leftwidth = two_leftwidth + WaaaghBar.plugins[i]:GetSize()
		elseif WaaaghBarSettings[BAR_TWO].OList.alignment[name] == "right" then -- Figure out the total width on right alignment and set the starting index
			two_rightwidth = two_rightwidth - WaaaghBar.plugins[i]:GetSize()
			if two_ri == 0 then	two_ri = i end
		elseif WaaaghBarSettings[BAR_TWO].OList.alignment[name] =="center" then -- Figure out the total width on center alignment and set the starting index
			two_centerwidth = two_centerwidth + WaaaghBar.plugins[i]:GetSize()
			if two_ci == 0 then two_ci = i end
		end
	end
			
	-- Set BAR_ONE Center and Right
	if one_ri ~= 0 then	-- actually set the right alignment plugins
		-- The loop end at the index of last BAR_ONE plugins
		for i = one_ri, getn(WaaaghBarSettings[BAR_ONE].OList.name) do
			local name = WaaaghBar.plugins[i].name
			one_rightwidth = one_rightwidth + WaaaghBar.plugins[i]:GetSize()
			WindowClearAnchors(name)
			WindowAddAnchor(name, "right", BAR_ONE, "right", one_rightwidth, 0)
			WindowSetShowing(name, true)
		end
	end
	if one_ci ~= 0 then -- actually set the center alignment plugins
		-- If no Alignment Right then end the loop like normal 
		-- and end at index of last BAR_ONE plugins
		if one_ri == 0 then one_ri = getn(WaaaghBarSettings[BAR_ONE].OList.name) else one_ri = one_ri - 1 end
		one_centerwidth = 0 - (one_centerwidth / 2)
		for i = one_ci, one_ri do
			local name = WaaaghBar.plugins[i].name
			WindowClearAnchors(name)
			WindowAddAnchor(name, "center", BAR_ONE, "left", one_centerwidth, 0)
			one_centerwidth = one_centerwidth + WaaaghBar.plugins[i]:GetSize()
			WindowSetShowing(name, true)
		end
	end
	
	-- Set BAR_TWO Center and Right
	if two_ri ~= 0 then	-- actually set the right alignment plugins
		for i = two_ri, getn(WaaaghBar.plugins) do
			local name = WaaaghBar.plugins[i].name
			two_rightwidth = two_rightwidth + WaaaghBar.plugins[i]:GetSize()
			WindowClearAnchors(name)
			WindowAddAnchor(name, "right", BAR_TWO, "right", two_rightwidth, 0)
			WindowSetShowing(name, true)
		end
	end
	if two_ci ~= 0 then -- actually set the center alignment plugins
		-- If no Alignment Right then end the loop like normal 
		-- and end at total number of plugins. If more bar needed
		-- two_ri must assign to index of last BAR_TWO plugins.
		if two_ri == 0 then two_ri = getn(WaaaghBar.plugins) else two_ri = two_ri - 1 end
		two_centerwidth = 0 - (two_centerwidth / 2)
		for i = two_ci, two_ri do
			local name = WaaaghBar.plugins[i].name
			WindowClearAnchors(name)
			WindowAddAnchor(name, "center", BAR_TWO, "left", two_centerwidth, 0)
			two_centerwidth = two_centerwidth + WaaaghBar.plugins[i]:GetSize()
			WindowSetShowing(name, true)
		end
	end
	
	-- Set Font
	WaaaghBar.SetBarFont(BAR_ONE, tostring(WaaaghBarSettings[BAR_ONE].font))
	WaaaghBar.SetBarFont(BAR_TWO, tostring(WaaaghBarSettings[BAR_TWO].font))
	
	WaaaghBar.SavePlugins()
end

--function WaaaghBar.ArrangePlugins()
--	local leftwidth = 0
--	local centerwidth = 0
--	local rightwidth = 0
--	local ci = 0
--	local ri = 0
--
--	WaaaghBar.MatchPlugins()
--	
--	for i = 1, getn(WaaaghBarSettings.PList.name) do	-- Code to remove selected plugins
--		if  not WaaaghBarSettings.PList.show[WaaaghBarSettings.PList.name[i]] then
--			WaaaghBar.DestroyPlugin(WaaaghBarSettings.PList.name[i])
--		end
--	end													-- END Code to remove selected plugins
--					
--	for i = 1, getn(WaaaghBarSettings.WList.name) do	-- Code to set preferred width
--		if WaaaghBar.GetElement(WaaaghBarSettings.WList.name[i]) then
--			WaaaghBar.GetElement(WaaaghBarSettings.WList.name[i]):SetSize(WaaaghBarSettings.WList.width[WaaaghBarSettings.WList.name[i]])
--		end
--	end													-- Code to set preferred width					
--
--	WaaaghBar.plugins = WaaaghBar.OrderPlugins() 
--			
--	for i = 1, getn(WaaaghBar.plugins) do	
--		local elementoffsetX = 0 			-- New Code: Resetting Anchors for Elements so that it's space correctly
--		for j = 2, getn(WaaaghBar.plugins[i].elements) do
--			local elementname = WaaaghBar.plugins[i].elements[j].name
--			WindowClearAnchors(elementname)
--			elementoffsetX = elementoffsetX + WaaaghBar.plugins[i].elements[j-1]:GetSize()
--			WindowAddAnchor(elementname, "left", WaaaghBar.plugins[i].name, "left", elementoffsetX, 0)
--			WindowSetShowing(elementname, true)
--		end 								-- END New Code: Resetting Anchors for Elements so that it's space correctly
--		local name = WaaaghBar.plugins[i].name
--		WindowClearAnchors(name)
--		if WaaaghBarSettings[BAR_ONE].OList.alignment[name] == "left" then
--			WindowAddAnchor(name, "left", BAR_ONE, "left", leftwidth, 0)
--			leftwidth = leftwidth + WaaaghBar.plugins[i]:GetSize()
--			WindowSetShowing(name, true)
--		elseif WaaaghBarSettings[BAR_ONE].OList.alignment[name] == "right" then -- Figure out the total width on right alignment and set the starting index
--			rightwidth = rightwidth - WaaaghBar.plugins[i]:GetSize()
--			if ri == 0 then	ri = i end
--		elseif WaaaghBarSettings[BAR_ONE].OList.alignment[name] =="center" then -- Figure out the total width on center alignment and set the starting index
--			centerwidth = centerwidth + WaaaghBar.plugins[i]:GetSize()
--			if ci == 0 then ci = i end
--		end
--	end
--	if ri ~= 0 then	-- actually set the right alignment plugins
--		for i = ri, getn(WaaaghBar.plugins) do
--			local name = WaaaghBar.plugins[i].name
--			rightwidth = rightwidth + WaaaghBar.plugins[i]:GetSize()
--			WindowAddAnchor(name, "right", BAR_ONE, "right", rightwidth, 0)
--			WindowSetShowing(name, true)
--		end
--	end
--	if ci ~= 0 then -- actually set the center alignment plugins
--		if ri == 0 then ri = getn(WaaaghBar.plugins) else ri = ri - 1 end -- If no Alignment Right then end the loop like normal
--		centerwidth = 0 - (centerwidth / 2)
--		for i = ci, ri do
--			local name = WaaaghBar.plugins[i].name
--			WindowAddAnchor(name, "center", BAR_ONE, "left", centerwidth, 0)
--			centerwidth = centerwidth + WaaaghBar.plugins[i]:GetSize()
--			WindowSetShowing(name, true)
--		end
--	end
--	WaaaghBar.SetAllFont(tostring(WaaaghBarSettings.font))
--	
--	WaaaghBar.SavePlugins()
--end

WaaaghBarPlugin = Frame:Subclass("WaaaghBarPlugin")

function WaaaghBarPlugin:New(name)
	self:CreateFromTemplate(name)
end

function WaaaghBar.GetPluginN(plugin)
	for i = 1, getn(WaaaghBar.plugins) do
		if WaaaghBar.plugins[i] == plugin then return i end
	end
end

WaaaghBarElement = Frame:Subclass("WaaaghBarElement")

function WaaaghBarElement:New(name)
	self:CreateFromTemplate(name)
end

WaaaghBar.plugin = {
	NewElement = function(self, name, width, text, color, icon, slash)
		local element = {
			plugin = self,
			name = self.name..name,
		}

		WaaaghBarElement:New(element.name)
		ins(self.elements, element)
		setmetatable(element, { __index = WaaaghBar.element })

		element:SetSize(width)
		element:SetText(text)
		element:SetColor(color)
		if icon then element:SetIcon(icon) end

		WindowClearAnchors(element.name)
		local offsetX = 0
		for i = 2, getn(self.elements) do -- Look through different element to set Anchor
			offsetX = offsetX + self.elements[i-1].width
		end
		
		WindowAddAnchor(element.name, "left", self.name, "left", offsetX, 0) -- Attach elements to WGB
		WindowSetShowing(element.name, true)

		-- SLASH
		if slash then WaaaghBar.slashHandlers[slash.abr] = slash.func end
		
		WindowSetParent(element.name, self.name) 
		
		return element
	end,
	Resize = function(self)
		local width = 0
		for i = 1, getn(self.elements) do
			width = width + self.elements[i].width
		end
		self.width = width
		WindowSetDimensions(self.name, width, 30)
	end,
	GetSize = function(self)
		return self.width
	end,
	SetSize = function(self, width)
		self.width = width
		WindowSetDimensions(self.name, width, 30)
	end
}

WaaaghBar.element = {
	SetSize = function(self, width)
		self.width = width
		WindowSetDimensions(self.name, width, 30)
		WindowSetDimensions(self.name.."Label", width, 30)
		self.plugin:Resize()
	end,
	SetText = function(self, text)
		LabelSetText(self.name.."Label", text)
	end,
	SetColor = function(self, color)
		LabelSetTextColor(self.name.."Label", color.r, color.g, color.b)
	end,
	SetIcon = function(self, icon)
		local texture, x, y = GetIconData(icon)
        DynamicImageSetTexture(self.name.."Icon", texture, x, y)
	end,
	SetIconScale = function(self,newScale) -- Just to match up Shock's API
		DynamicImageSetTextureScale(self.name.."Icon", newScale)
	end,
	GetIcon = function(self, icon) -- Just to match up Shock's API
			return self.icon
	end,
	SetFont = function(self, font)
		LabelSetFont(self.name.."Label", font, WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
	end,
	GetSize = function(self)
		return self.width
	end
}